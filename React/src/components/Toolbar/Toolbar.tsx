import React from 'react';
import './Toolbar.css';
import DrawerToggleButton from '../SideDrawer/DrawerToggleButton';
import FriendList from '../FriendList/FriendList';
import { Link } from 'react-router-dom';
interface Props {
    drawerClickHandler: any;
    show: any;
    showSearch: any;
    resoultsClass: any;
    showResoults: any;
    resoults: any;
    elClick: any;
}

const toolbar = (props: Props) => {
    
    let searchHide = 'input';
    let resoultsHide = 'resoults';
    if( !props.show) {
        searchHide = 'input close';
    }
    if( !props.resoultsClass) {
        resoultsHide = 'resoults close';
    }
    return(
    <header className="toolbar">
        <nav className="toolbar_navigation">
            <div>
                <DrawerToggleButton click={props.drawerClickHandler}/>
            </div>
            <div className="toolbar_logo"><Link to={`/home`}>MY FACE</Link></div>
            <div className="spacer"></div>
            <input type="text" onChange={(event) => props.showResoults(event)} className={searchHide}></input>
            <div className={resoultsHide} >
                <FriendList Route={true} frends={props.resoults} itemClick={props.elClick} />
            </div>
            <div className="toolbar_navigation_items">
                <ul>
                    <li><i onClick={props.showSearch} className="fas fa-search"></i></li>
                </ul>
            </div>
        </nav>
    </header>
);}

export default toolbar;  