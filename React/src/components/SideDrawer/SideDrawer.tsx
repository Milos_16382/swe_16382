import React from 'react';
import './SideDrawer.css';
import Avatar from '../Avatar/Avatar';
import FriendList from '../FriendList/FriendList';
import { Link } from 'react-router-dom';
interface Props{
    show: boolean;
    user: any;
    frends: any;
    frendClickHandler: any;
}

const sideDrawer = (props: Props) => {

    let drawerClasses = 'side-drawer';

    if( props.show) {
        drawerClasses = 'side-drawer open'
    }
    return (
        <div className={drawerClasses}>
            <div className="drawer-profile">
            <Link to={`home/${props.user.id}`}>
                <Avatar size="big" link={props.user.userPic}/>
            </Link>
            
            <div className="line"></div>
            <strong>{props.user.name}</strong>
            </div>
                <FriendList Route={false} itemClick={props.frendClickHandler} frends={ props.frends }/>
                <Link to={"./"}><p>Logout</p></Link>
        </div>
    );
}


export default sideDrawer;