
export class EmptyEl {

    constructor() {
        let elContainer = document.createElement("div");
      elContainer.className = "question-container";
      let questionP = document.createElement("p");
      questionP.className = "question";
      let infoContainer = document.createElement("div");
      infoContainer.className = "info";
      let devider = document.createElement("div");
      devider.className = "spacer";
      let nameP = document.createElement("p");
      nameP.className = "user-name";
      let dateP = document.createElement("p");
      dateP.className = "question-date";

      
        this.con = elContainer;
        this.question = questionP;
        this.info = infoContainer;
        this.devider = devider;
        this.name = nameP;
        this.date = dateP;
      
    }
}