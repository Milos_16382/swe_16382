import { func } from "prop-types";

export function fetchChatUser (id: number){
    return (dispatch: any) => {
       fetch(`http://localhost:3000/user/${id}`)
        .then( res => res.json())
        .then ( (user) => 
            dispatch( {
            type: "SET_CHAT_USER",
            payload: user
        } )
    )
    }
}
export function fetchChat(myId: number, frendId: number) {
    return (dispatch: any) => {
        let Today = new Date();
        fetch(`http://localhost:3000/messages?messageIndex=${myId < frendId ? parseInt(`${myId}${frendId}`): parseInt(`${frendId}${myId}`)}&messageDay_lte=${Today.getDate()}&messageMonth=${Today.getMonth()}&messageYear=${Today.getFullYear()}&_sort=id&_order=desc&_limit=50`)
         .then( res => res.json())
         .then ( (chat) => dispatch({
             type: "SET_CHAT",
             payload: chat
         }))
     }
}
export function updateChat( myId: number, frendId: number, id: number) {
    return (dispatch: any) => {
        fetch(`http://localhost:3000/messages?messageIndex=${myId < frendId ? parseInt(`${myId}${frendId}`): parseInt(`${frendId}${myId}`)}&id_lte=${id-1}&_limit=50`)
         .then( res => res.json())
         .then ( (chat) => dispatch({
             type: "APPEND_CHAT",
             payload: chat
         }))
     }
}

export function sendMessage(id: number, fId: number, message: string) {
    return (dispatch: any) => {
        let Today = new Date();
        fetch('http://localhost:3000/messages', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            messageIndex: id < fId ? parseInt(`${id}${fId}`): parseInt(`${fId}${id}`) ,
            myId: id,
            frendId: fId,
            message: message,
            sender: id,
            messageDay: Today.getDate(),
            messageMonth: Today.getMonth(),
            messageYear: Today.getFullYear(),
            messageTime: parseInt(`${Today.getHours()}${Today.getMinutes()}`)
          })
        })
         .then( res => res.json())
         .then ( (message) => 
            dispatch({
                type: "APPEND_CHAT",
                payload: message
            })
        )
     }
}