import React from 'react';
import './ChatMsg.css'
import Avatar from '../../Avatar/Avatar';

interface Props {
    person: string;
    url: string;
    message:string;   
}
const chatMsg = (props: Props) => (
    <div className={props.person}>
                <Avatar size="small" link={props.url} />
                <p className="chat-message"> {props.message}</p>
    </div>
);

export default chatMsg;