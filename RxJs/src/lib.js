import { handelClickForProfesor } from "./Profesor/Handlers";
import { EmptyEl } from "./Profesor/EmptyEl";

export function convertNmbToDate(number) {
    const dateStr = number.toString();
    let date = '' ;
    date = date.concat( dateStr.slice(dateStr.length-8, dateStr.length-6));
    date = date.concat("/");
    date = date.concat( dateStr.slice(dateStr.length-6, dateStr.length-4));
    date = date.concat("/");
    date = date.concat( dateStr.slice(dateStr.length-4, dateStr.length));
    return date;
}

export function fillElement(pitanjaArray, name) {
    let html;
    pitanjaArray.forEach( pitanja => {
        html = new EmptyEl();
        fillHtmlElement(html,pitanja, name, handelClickForProfesor)
    });
}
export function fillHtmlElement(html,pitanja, name, clickHandler) {
    const container = document.getElementsByClassName("background")[0];
        html.name.innerHTML = name;

        html.date.innerHTML = convertNmbToDate(pitanja.date);
        html.info.appendChild(html.name);
        html.info.appendChild(html.devider);
        html.info.appendChild(html.date);
        html.question.innerHTML = pitanja.question;
        html.con.appendChild(html.question);
        html.con.appendChild(html.info);
        container.insertBefore(html.con, container.firstChild);
        clickHandler(html.con, pitanja);
        //handelClickForProfesor(html.con, pitanja);
}
export function fetchPostCall( url, body) {
   return fetch(`${url}`, {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
      }).then(res => res.json())
}