import React from 'react';
import './Backdrop.css';

interface Props {
    click: any;
}

const backdrop = (props: Props) => (
    <div className="backdrop" onClick={props.click}></div>
)

export default backdrop;