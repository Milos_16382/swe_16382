import {
  interval,
  range,
  Observable,
  Subscription,
  Subject,
  fromEvent,
  from,
  of,
  zip
} from "rxjs";
import {
  take,
  filter,
  map,
  takeUntil,
  sampleTime,
  debounceTime,
  switchMap,
  pluck,
  distinctUntilChanged,
  mergeMap,
  tap
} from "rxjs/operators";
import { EmptyEl } from "./EmptyEl";
import { fetchPostCall } from "../lib";

const url = 'http://localhost:3000';


export function setBackdropHandler() {
  const backdrop = document.getElementsByClassName("backdrop")[0];

  if (backdrop !== undefined) {
    backdrop.onclick = event => {
      backdrop.className = " backdrop closed";
      document.getElementsByClassName("modal")[0].className = "modal closed";
    };
  }
}
export function setAdminBtnHandler() {
  const adminBtn = document.querySelector(".admin");

  if (adminBtn !== null) {
    adminBtn.onclick = event => {
      const singInInput = document.getElementsByClassName("sing-in")[0];
      if (singInInput.className === "sing-in") {
        singInInput.className = "sing-in hiden";
      } else {
        singInInput.className = "sing-in";
      }
    };
  }
}

export function getMyQuestions(name) {
  const sub = new Subject();

  fetch(`${url}/pitanja?userName=${name}`)
    .then(response => response.json())
    .then(data => sub.next(data));

  return sub;
}
export function getUser(name) {
  
  return from(fetch(`${url}/korisnici?name=${name}&_limit=1`)
    .then(response => response.json())
    //.then(data => data))
  )
}
export function setNameInputHandler() {
  const nameInput = document.querySelector(".sing-in");

  if (nameInput !== null) {
    nameInput.onkeypress = event => {
      if (event.key === "Enter") {
        fetch(`${url}/korisnici?name=${event.target.value}`)
          .then(response => response.json())
          .then(user => {
            if (user[0] === undefined) {
              fetchPostCall(`${url}/korisnici`, {name: event.target.value})
                .then(
                  newUser =>
                    (window.location.href = `./Admin.html?name=${newUser.name}`)
                );
            } else {
              window.location.href = `./Admin.html?name=${event.target.value}`;
            }
          });
      }
    };
  }
}
export function handelClickForProfesor(div, data) {
  div.onclick = event => {
    document.getElementsByClassName("backdrop")[0].className = "backdrop";

    const quest = document.getElementsByClassName("question-modal")[0];
    const modal = document.getElementsByClassName("modal")[0];
    const odgovori = document.getElementsByClassName("a-container")[0];

    while (odgovori.firstChild) {
      odgovori.removeChild(odgovori.firstChild);
    }
    getResoults(data.id)
      .pipe(
        map( obj => obj[0].resoults),
        //switchMap(obj => from(obj[0].resoults)),
        map((res, indx) => {
          console.log(res);
          console.log(data.answers);
          data.answers.forEach((answer, index) => {
              let option = document.createElement("p");
              option.innerHTML = answer + " : " + res[index];
              odgovori.appendChild(option);
          });
          return res;
        }),
        tap(x => { 
        let option = document.createElement("p");
        option.innerHTML = 'ukupan broj prijava: ' + x.reduce( (acu, val) => acu += val, 0);
        odgovori.appendChild(option);
      })
      )
      .subscribe();
    quest.innerHTML = data.question;
    modal.className = "modal";
  };
}

function getResoults(id) {
  return from(
    fetch(`${url}/odgovori?qId=${id}`).then(response =>
      response.json()
    )
  );
}


export function getEmptyEl() {
  return Observable.create(obs => {
    setInterval(() => obs.next(new EmptyEl()), 100);
  });
}

export function setAddAnswHandler() {
  const addAnswBtn = document.querySelectorAll(".buttons button")[1];

  if (addAnswBtn !== null) {
    addAnswBtn.onclick = event => {
      const answTxtArea = document.createElement("textarea");
      const form = document.getElementsByClassName("form")[0];
      answTxtArea.className = "odgovor temp";
      form.appendChild(answTxtArea);
    };
  }
}

export function setMakeQHandler(name, qArray) {
  const makeQ = document.querySelectorAll(".buttons button")[0];

  if (makeQ !== null) {
    makeQ.onclick = event => {
      const question = document.getElementsByClassName("pitanje")[0];
      const odgovori = document.getElementsByClassName("odgovor");

      let odgovoriArray = [];
      for( let i = 0 ; i < odgovori.length ; i++){
        odgovoriArray[i] = odgovori[i].value;
      }
      
      let Today = new Date();
      fetchPostCall(`${url}/pitanja`,{
        answers: odgovoriArray,
        question: question.value,
        userName: name,
        date: parseInt(`${Today.getDate()  / 10 < 1 ? 0 : ""}${Today.getDate()}${Today.getMonth() / 10 < 1 ? 0 : ""}${Today.getMonth()}${Today.getFullYear()}`),time: parseInt(`${Today.getHours()}${Today.getMinutes()}`)
      } ).then(data => {
          qArray.next([data]);
          let resArray = [];
          data.answers.forEach(() => resArray.push(0));
          fetchPostCall(`${url}/odgovori`, {
            qId: data.id,
            resoults: resArray
          });
        });

      for (var i = 0; i < odgovori.length; i++) {
        odgovori[i].value = "";
      }
      question.value = "";
    };
  }
}
