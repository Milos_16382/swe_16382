import React from 'react';
import './Galery.css'

interface Props{
    photos: any;
}
const galery = (props: Props) =>{ 
    
    let photos = props.photos.map( (photo: any) =>(<img src={photo.picUrl} key={photo.id}/>) );
    
    return(
    <div className="galery">
       {photos}
    </div>
);
}
export default galery;