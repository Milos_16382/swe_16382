import {intervel, forkJoin,timer,from,interval, zip} from "rxjs";
import {map, pairwise,filter, scan} from "rxjs/operators";


const $obs1 = timer(1000).pipe(
    map( i => 'ob 1')
)
const $obs2 = timer(3000).pipe(
    map( i => 'ob 2')
)

forkJoin($obs1,$obs2)
.subscribe(x => console.log(x))

from([1,5,2,6,8,4,4])
    interval(500).pipe(
    
    pairwise(),
    filter(pair => pair[0] === pair[1]),
    map(pair =>pair[0])
        
  
)
.subscribe(x => console.log(x))

const $niz = from([1,5,5,2,8,4,4]);
const $timer = interval(300);
zip($niz,$timer).pipe(
    
    pairwise(),
    filter(pair => pair[0][0] === pair[1][0]),
    map(pair =>pair[0][0]),
    scan((acc,value)=> acc + value)
)
.subscribe(x => console.log(x))