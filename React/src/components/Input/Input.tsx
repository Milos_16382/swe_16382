import React from 'react';
import './Input.css';

interface Props {
    placeHoleder: string;
    type: string;
    label: string;
    handler: Function;
}

const input = (props: Props)  => (
    <div className="input-style">
        <p>{props.label}</p>
        <input type={props.type} placeholder={props.placeHoleder} onInputCapture={ (event) => props.handler(event) } />
        <div className="line" />
    </div>
);


export default input;

