import { Action } from '@ngrx/store';

export const GET_CARDS = ' get cards';
export const GET_CARDS_SUCCESS = 'get cards success';
export const CLEAR_CARDS = ' clear cards';
export const ADD_CARD = ' add card';
export const DELETE_CARD = ' delete card';
export const UPDATE_CARD = ' update card';
export const LEARN_CARD = ' learn card';

export class GetCards implements Action {
    readonly type = GET_CARDS;
    constructor(public payload: number) {}
}

export class GetCardsSuccess implements Action {
    readonly type = GET_CARDS_SUCCESS;
    constructor(public payload: Card[]) {}
}

export class ClearCards implements Action {
    readonly type = CLEAR_CARDS;
}
export class AddCard implements Action {
    readonly type = ADD_CARD;
    constructor(public payload: {
      card: Card,
      id: number
    }) {}
}
export class DeleteCard implements Action {
    readonly type = DELETE_CARD;
    constructor(public payload: number) {}
}

export class UpdateCard implements Action {
    readonly type = UPDATE_CARD;
    constructor(public payload: {
        card: Card,
        id: number
    }) {}
}
export class LearnCard implements Action {
    readonly type = LEARN_CARD;
    constructor(public payload: {
     card: Card,
     id: number
    }) {}
}
export type CardsAction = GetCards | GetCardsSuccess | AddCard | DeleteCard
| UpdateCard | LearnCard;
