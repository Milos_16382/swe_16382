
import{zip} from "rxjs";
import{tap} from "rxjs/operators";
import {setNameInputHandler,setAdminBtnHandler, getMyQuestions, handelClick, setBackdropHandler
    , setAddAnswHandler, setMakeQHandler,handelClickForProfesor, getUser} from './Profesor/Handlers';
import {guestSearchHandler} from './Student/Handlers';
import { EmptyEl } from "./Profesor/EmptyEl";
import { getSearchParameters } from "./getParams";
import { fillElement } from "./lib";
import { initGame } from "./Game/Game";



var params = getSearchParameters();
const main = document.querySelector(".main");
if(params.name !== undefined){
    setBackdropHandler();
    setAddAnswHandler();
    let myQuestions = getMyQuestions(params.name)
    setMakeQHandler(params.name, myQuestions);
    const userObs = getUser(params.name);
     zip(myQuestions, userObs).pipe(
            tap( el => fillElement(el[0], el[1][0].name) ),
    ).subscribe();
    //     myQuestions.pipe( 
    //     switchMap( array => from(array)),
    //     mergeMap( el => getEmptyEl().pipe(
    //         take(1),
    //         map( html => {
    //             const container = document.getElementsByClassName("background")[0];
    //             html.name.innerHTML = el.userName;

    //             const dateStr = el.date.toString();
    //             let date = '' ;
    //             date = date.concat( dateStr.slice(dateStr.length-8, dateStr.length-6));
    //             date = date.concat("/");
    //             date = date.concat( dateStr.slice(dateStr.length-6, dateStr.length-4));
    //             date = date.concat("/");
    //             date = date.concat( dateStr.slice(dateStr.length-4, dateStr.length));

    //             html.date.innerHTML = date;
    //             html.info.appendChild(html.name);
    //             html.info.appendChild(html.devider);
    //             html.info.appendChild(html.date);
    //             html.question.innerHTML = el.question;
    //             html.con.appendChild(html.question);
    //             html.con.appendChild(html.info);
    //             container.insertBefore(html.con, container.firstChild);
    
    //             handelClickForProfesor(html.con, el);
    //         })
    //     )),
    // ).subscribe();
}else if(main != null){
    
    initGame();
}else {

    const textBox = document.querySelector('.search');

    if( textBox !== null){
        guestSearchHandler();
        setBackdropHandler();
    }
}

setAdminBtnHandler();
setNameInputHandler();
