import * as fromCards from '../actions/card.action';
export interface CardState {
   cards: {[id: number]: Card};
}

export const initialState: CardState = {
    cards: {}
};

export function reducer(state = initialState, action: fromCards.CardsAction): CardState {

    switch (action.type) {

        case fromCards.GET_CARDS_SUCCESS: {
            const cardsArray = action.payload;
            const cards = cardsArray.reduce( (cardss, card: Card) => {
                return {
                    ...cardss,
                    [card.id] : card
                };
            },
            { }
            );
            return {
                ...state,
                cards,
            };
        }
        // case fromCards.GET_CARDS_SUCCESS: {

        //     return {
        //         ...state,
        //         cards: []
        //     };
        // }
        default: {
            return state;
        }
    }
}

export const getCards = (state: CardState) => state.cards;
