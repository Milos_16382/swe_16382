import { Injectable } from '@angular/core';
import {Effect, Actions, ofType} from '@ngrx/effects';
import * as userActions from '../actions/user.action';
import { switchMap, map } from 'rxjs/operators';
import { UserServiceService } from 'src/app/user-service.service';

@Injectable()
export class UserEffects {
    constructor(private actions$: Actions, private userService: UserServiceService) {}

    @Effect()
    getUser$ = this.actions$.pipe(
        ofType(userActions.GET_USER),
        switchMap( (action: userActions.GetUser) => {
            return this.userService.getUser(action.payload.index, action.payload.fax).pipe(
                map( user => new userActions.GetUserSuccess(user))
            );
        })
    );

    @Effect({ dispatch: false })
    getUpdatedUser$ = this.actions$.pipe(
        ofType(userActions.UPDATE_USER),
        map( (action: userActions.UpdateUser) => {
            return this.userService.updateUser(action.payload);
        })
    );

    @Effect()
    getSearchUserss$ = this.actions$.pipe(
        ofType(userActions.SEARCH_USER_NAME),
        switchMap( (action: userActions.SearchUserName) => {
            return this.userService.searchName(action.payload).pipe(
                map( users => new userActions.SearchUserNameSuccess(users))
            );
        })
    );

}
