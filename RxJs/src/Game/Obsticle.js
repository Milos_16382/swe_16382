

export class Obsticle {


    constructor() {

        this.obsticle = document.createElement("div");
        this.obsticle.className = 'obsticle hide';
        
        this.setMargine();
        this.setRotation();
    }


    setSize() {
        this.obsticle.style.height = '200px';
        this.obsticle.style.width = '200px';
    }

    setMargine() {
        let rnd = (Math.random() * 1000);
        this.obsticle.style.marginLeft = `${rnd}px`;
        rnd = (Math.random() * 500);
        this.obsticle.style.marginTop = `${rnd}px`;
    }

    setRotation() {
        let rnd = Math.floor(Math.random() * 360);
        this.obsticle.style.transform = `rotate(${rnd}deg)`;
    }



}