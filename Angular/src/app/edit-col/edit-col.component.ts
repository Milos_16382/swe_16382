import { Component, OnInit } from '@angular/core';
import { ColectionServiceService } from '../colection-service.service';
import { PopoverController } from '@ionic/angular';
import * as fromStore from '../store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-edit-col',
  templateUrl: './edit-col.component.html',
  styleUrls: ['./edit-col.component.scss'],
})
export class EditColComponent implements OnInit {

  colName: string;
  colVisable = false;
  colection: Colection;
  constructor(private store: Store<fromStore.ColectionAction>, private popCntrl: PopoverController) { }

  ngOnInit() {}

  setName(ev) {
    this.colName = ev.target.value;
  }

  setVisable(ev) {
    this.colVisable = ev.target.value;
  }
  change() {

    this.store.dispatch(new fromStore.EditColection({
      Name: this.colName,
      Visable: this.colVisable,
      Likes: this.colection.Likes,
      Liked: this.colection.Liked,
      id: this.colection.id,
      IndexUser: this.colection.IndexUser,
      FaxUser: this.colection.FaxUser
    }));
    this.popCntrl.dismiss();
  }
  cancle() {
  this.popCntrl.dismiss();
  }
}
