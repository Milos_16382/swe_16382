import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Color } from '@ionic/core';
import * as fromStore from '../../store/index';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.page.html',
  styleUrls: ['./add-form.page.scss'],
})
export class AddFormPage implements OnInit {


  constructor(private store: Store<fromStore.CBState>, private router: Router,  private toastCtrl: ToastController) { }

  name = '';
  visable: boolean;
  errName: Color = 'danger';
  isDisabled = true;
  user$: Observable<User>;
  ngOnInit() {
    this.user$ = this.store.select( fromStore.getUser);
  }
  nameCheck() {
    if (this.name !== undefined && this.name !== '') {
        this.errName = 'primary';
        this.isDisabled = false;
    }
  }


  addColection() {
    this.user$.subscribe( user => {
       this.store.dispatch(new fromStore.AddColection({
        Name: this.name,
        Visable: this.visable,
        Liked: 'heart-empty',
        Likes: 0,
        IndexUser: user.id,
        FaxUser: user.Fax,
      }));

    });
    this.swipeToast();

    this.router.navigate(['../']);
  }
  async swipeToast() {
    const toast = await this.toastCtrl.create({
      message: '  Swipe up to Refresh the list \n Swipe the Clolection left to Delete or right to Edit',
      duration: 7000,
      position: 'bottom'
    });
    toast.present();
  }
}
