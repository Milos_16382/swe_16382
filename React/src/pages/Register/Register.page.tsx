import React,{ Component } from "react";
import { Route } from "react-router";
import './Register.page.css';

import Input from '../../components/Input/Input';
import { connect } from "react-redux";
import { registerUser } from "../../store/actions/UserActions";

interface RegState{
    name: string,
    gmail: string,
    pass: string,
    rePass: string;
  
}
interface Props{
    Register: any;
}
class Register extends Component<Props> {

    state : RegState = {
        name: '',
        gmail: '',
        pass: '',
        rePass: ''
    }
    nameHandler = (event : any) =>{
        this.setState({ name : event.target.value} );
    }
    gmailHandler = (event : any) =>{
        this.setState({ gmail : event.target.value} );
    }
    passHandler = (event : any) =>{
        this.setState({ pass : event.target.value} );
    }
    rePassHandler = (event : any) =>{
        this.setState({ rePass : event.target.value} );
    }

   
   
    render() {

        return (
                <div className="register-page">
                <img src="https://wallpapermemory.com/uploads/654/purple-wallpaper-hd-1920x1080-405334.jpg" />
                <div className="register-card">
                    <Input placeHoleder={"Ljubisa"} label="Name" type="text" handler={this.nameHandler}/>
                    <Input placeHoleder={"example@gmail.com"} label="Gmail" type="text" handler={this.gmailHandler}/>
                    <Input placeHoleder={""} label="Password" type="password" handler={this.passHandler} />
                    <Input placeHoleder={""} label="Re Password" type="password" handler={this.rePassHandler}/>
                    <Route render={({ history}) => (
                        <button
                            type='button'
                            onClick={() => { 
                                this.props.Register( this.state.name,this.state.pass,this.state.gmail,);
                                history.push('/'); }}
                            >
                            Register
                        </button>
                    )} />
                    <Route render={({ history}) => (
                        <button
                            type='button'
                            onClick={() => { history.push('/') }}
                            >
                            Back
                        </button>
                    )} />
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state: any) => {
    return {
    };
  };
  
  const mapDispatchToProps = (dispatch: any) => {
    return {
      Register: (name: string, pass: string, gmail: string) => {
          dispatch(registerUser(name,pass,gmail));
      }
    };
  };
  
   export default connect(mapStateToProps, mapDispatchToProps)(Register);
