import React from 'react';
import Avatar from '../../Avatar/Avatar';
import './FriendListEl.css'
import { Route } from 'react-router';

interface Props {
    img: any;
    name: string;
    id: number;
    ClickHandler: any;
    key: number;
    route: boolean;
    sId: number;
}
const friendListEl = (props: Props) => {
    
    let link;
    if( props.route === true){
        link = <Route render={({ history}) => (
             <p onClick={() =>  history.push(`/home/${props.sId}`)} >{props.name}</p>
        )} />
       // link = <p onClick={() => props.ClickHandler()}>{props.name}</p>
    }else {
        link = <p onClick={() => props.ClickHandler(props.id)}>{props.name}</p>
    }
    return(
    <div className="friend-list-el">
                <div className="list-spacer" />
                    <Avatar size="small" link={props.img}/>
                    {link}
                <div className="list-spacer" />
    </div>
 );
}

 export default friendListEl;
