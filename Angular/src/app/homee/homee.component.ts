import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ColectionServiceService } from '../colection-service.service';
import { ToastController, AlertController, PopoverController } from '@ionic/angular';
import { UserServiceService } from '../user-service.service';
import { Observable } from 'rxjs';
import { UserDataComponent } from '../user-data/user-data.component';
import { EditColComponent } from '../edit-col/edit-col.component';
import { Store } from '@ngrx/store';
import * as fromStore from '../store';
import { skip } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homee',
  templateUrl: 'homee.component.html',
  styleUrls: ['homee.component.scss'],
})
export class HomeeComponent implements OnInit {

  search: Observable<boolean>;
  colection$: Observable<Colection[]>;

  @Input()
  user$: Observable<User>;
  @Input()
  searching: boolean;
  @Output() clearSearch = new EventEmitter();
  searchUser$: Observable<User>;
  like: 'heart';
  constructor( private toastCtrl: ToastController,
               private alertController: AlertController, private popoverController: PopoverController,
               private store: Store<fromStore.ColectionAction>, private router: Router) {


  }
  async ngOnInit() {

    if (this.searching === false) {
      this.store.dispatch(new fromStore.SetSearching(false));
    } else {
      this.store.dispatch(new fromStore.SetSearching(true));
    }

    this.search = this.store.select(fromStore.getSearching);
    this.searchUser$ = this.store.select( fromStore.getSUser);

    this.colection$ = this.store.select(fromStore.getColections);

    this.user$.subscribe(user => {
      this.store.dispatch( new fromStore.GetColections({
      index: user.id,
      fax: user.Fax
    }));
  });

    this.colection$.pipe(
    skip(1)
    ).subscribe( col => this.searching === false ? this.loginToast(col.length) : null);


  }

  async loginToast(col) {

    if (col === 0) {
      const toast = await this.toastCtrl.create({
        message: 'Click the plus sign to add your first collection',
        duration: 7000,
        position: 'bottom'
      });
      toast.present();
    }
  }
  delColection(id: number) {
    this.deleteAlert(id);
  }
  doRefresh(event) {
    this.user$.subscribe(user => this.store.dispatch( new fromStore.GetColections({
      index: user.id,
      fax: user.Fax
    })));
    event.target.complete();
  }

  likeCol(id: number, liked: string) {

    this.user$.subscribe( user => {

      this.store.dispatch( new fromStore.Like({
        colId: id ,
        colLiked: liked,
        uIndx: user.id,
        ufax: user.Fax
      }));
    });

  }

  async deleteAlert(i: number) {
    const alert = await this.alertController.create({
      header: 'Are you sure!',
      message: '<strong>you want to delete this colection</strong>!!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary'

        }, {
          text: 'Yes',
          handler: () => {
            this.store.dispatch( new fromStore.DeleteColection(i));

          }
        }
      ]
    });

    await alert.present();
  }
  makeChange() {
      if (this.searching === false) {
      this.user$.subscribe( user => this.presentPopover(user.Name, user.Password, user.PicUrl));
    }

  }

  async presentPopover(name: string, password: string, url: string) {
    const popover = await this.popoverController.create({
      component: UserDataComponent,
      componentProps: {
        namePlace: name,
        pass: password,
        urlPlace: url
      },
      translucent: true
    });
    return await popover.present();
  }
  async editColection(col: Colection) {
    const popover = await this.popoverController.create({
      component: EditColComponent,
      componentProps: {
        colection: col
      },
      translucent: true
    });
    return await popover.present();
  }
  getFax(x) {

        switch (x) {
          case 1:
            return 'Elfak';
            break;
          case 2:
            return 'FF';
            break;
          case 3:
            return 'DIF';
            break;
        }
  }
  searchCheck(): boolean {
    let check: boolean;
    this.store.select(fromStore.getSearching).subscribe(search => {
      if (search === false) {
        check = true;
      } else {
        check = false;
      }
    });
    return check;
  }
  backToSearch() {

    if ( this.searching === true) {
      this.clearSearch.emit();
    }
  }

  goToSearch() {
    this.router.navigate(['../search']);
  }
}
