import * as fromUser from './user.reducer';
import * as fromCol from './colection.reducer';
import * as fromCards from './cards.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';

export interface CBState {
    user: fromUser.UserState;
    colections: fromCol.ColectionState;
    cards: fromCards.CardState;
}

export const reducers: ActionReducerMap<CBState> = {
    user: fromUser.reducer,
    colections: fromCol.reducer,
    cards: fromCards.reducer
};

export const getCBState = createFeatureSelector<CBState>('');

export const getUserState = createSelector(getCBState, (state: CBState) => state.user);
export const getColectionsState = createSelector(getCBState, (state: CBState) => state.colections);
export const getCardsState = createSelector(getCBState, (state: CBState) => state.cards);

export const getUser =  createSelector(getUserState, fromUser.getUser);
export const getUserLoaded =  createSelector(getUserState, fromUser.getUserLoaded);
export const getLoading =  createSelector(getUserState, fromUser.getUserLoading);
export const getSearching =  createSelector(getUserState, fromUser.getSearching);
export const getLoginUser =  createSelector(getUserState, fromUser.getLogin);
export const getSUsers =  createSelector(getUserState, fromUser.getSearchUsers);
export const getSUser =  createSelector(getUserState, fromUser.getSearchUser);

export const getColectionEntities = createSelector(getColectionsState, fromCol.getColections);
export const getColections = createSelector(getColectionEntities, (entities) => {
    return Object.keys(entities).map( id => entities[parseInt(id , 10)]);
});


export const getComentEntities = createSelector(getColectionsState, fromCol.getComents);
export const getComents = createSelector(getComentEntities,  (entities) => {
    return Object.keys(entities).map( id => entities[parseInt(id , 10)]);
});

export const getCardEnities = createSelector(getCardsState, fromCards.getCards);

export const getCards = createSelector(getCardEnities, (entities) => {
    return Object.keys(entities).map( id => entities[parseInt(id , 10)]);
});
