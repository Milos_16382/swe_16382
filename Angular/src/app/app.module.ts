import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CardBackComponent } from './card-back/card-back.component';
import { CardEditComponent } from './card-edit/card-edit.component';
import { UserDataComponent } from './user-data/user-data.component';
import { UrlComponent } from './url/url.component';
import { EditColComponent } from './edit-col/edit-col.component';
import { StoreModule } from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {reducers, effects} from './store/index';
import { UserServiceService } from './user-service.service';
import { ColectionServiceService } from './colection-service.service';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HomeeComponent } from './homee/homee.component';
import { SearchPageModule } from './home/search/search.module';
import { LoginPageModule } from './login/login.module';
import { HomeeModule } from './homee/homee.module';

@NgModule({
  declarations: [AppComponent, CardBackComponent, CardEditComponent, UserDataComponent, UrlComponent, EditColComponent],
  entryComponents: [CardBackComponent, CardEditComponent, UserDataComponent, UrlComponent, EditColComponent],
  imports: [BrowserModule,
  HomeeModule,
  LoginPageModule,
  HttpClientModule,
  IonicModule.forRoot(),
  AppRoutingModule,
  StoreModule.forRoot({}),
  StoreModule.forFeature('', reducers),
  EffectsModule.forRoot([]),
  EffectsModule.forFeature(effects),  
  StoreDevtoolsModule.instrument({
    maxAge: 25, // Retains last 25 states
     // Restrict extension to log-only mode
  }),
  ],
  providers: [
    UserServiceService,
    ColectionServiceService,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
