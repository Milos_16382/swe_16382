import { func } from "prop-types";

export function fetchUser (gmail: string){
    return (dispatch: any) => {
       fetch(`http://localhost:3000/user?gmail=${gmail}`)
        .then( res => res.json())
        .then ( (user) => dispatch( {
            type: "SET_USER",
            payload: user[0]
        } ))
    }
}


export function fetchPosts (myId:number ,length: number, index: number ,userId: number, day: number, month: number, year: number, reWrite:boolean,lastId: number){
    
    return (dispatch: any) => {

        if( reWrite === false) {
            fetch(`http://localhost:3000/posts?userId=${userId}&postDay=${day}&postMonth=${month}&postYear=${year}&_limit=100&_sort=id&_order=desc`)
            .then( res => res.json())
            .then ( posts =>{
                if(posts[0] !== undefined){
                        fetchPostNameAndPic(myId,posts, dispatch,reWrite,index);
                        }
            })
        }
        else{
            fetch(`http://localhost:3000/posts?userId=${userId}&id_lte=${lastId === 0 ? 9999999 : lastId - 1}&_limit=100&_sort=id&_order=desc`)
            .then( res => res.json())
            .then ( posts =>{
                if(posts[0] !== undefined){
                        fetchPostNameAndPic(myId,posts, dispatch,reWrite,index);
                        }
            })
        }
            
                // if(day - 1 > 0){
                //     fetch(`http://localhost:3000/posts?userId=${userId}&postDay_lte=${day-1}&postMonth=${month}&postYear=${year}&_limit=100`)
                //     .then( res => res.json())
                //     .then ( posts =>{
                //         if(posts[0] !== undefined){
                //                 fetchPostNameAndPic(posts, dispatch,reWrite,index);
                //                 }
                //     })
                // }
                // else{
                //     if(month -1 > 0){
                //         fetch(`http://localhost:3000/posts?userId=${userId}&postDay_lte=${31}&postMonth=${month-1}&postYear=${year}`)
                //     .then( res => res.json())
                //     .then ( posts =>{
                //         if(posts[0] !== undefined){
                //                 fetchPostNameAndPic(posts, dispatch,reWrite,index);
                //                 }
                //     })
                //     }else {
                //         fetch(`http://localhost:3000/posts?userId=${userId}&postDay_lte=${31}&postMonth=${12}&postYear=${year-1}`)
                //         .then( res => res.json())
                //         .then ( posts =>{
                //             if(posts[0] !== undefined){
                //                     fetchPostNameAndPic(posts, dispatch,reWrite,index);
                //                     }
                //         })
                //     }
                // }

        if(length === index){
            dispatch( {
                type: "SET_POST_UPDATE"
            });
        }
    }
}

export function fetchFrends (id: number, day: number, month: number, year: number){
    return (dispatch: any) => {
        fetch(`http://localhost:3000/frends?myId=${id}`)
        .then( res => res.json())
        .then ( frends => {
            
         
        frends.map( (frend: any) =>  {
        let newFrend  = fetch(`http://localhost:3000/user/${frend.frendId}`)
        .then( res => res.json())
        .then ( user => frend = {
            ...frend,
            userPic : user.userPic,
            name : user.name,
            id : user.id
        }
        );
        newFrend.then( data => dispatch( {
            type: "SET_FREND",
            payload: data
        })
        );
    })
        
        frends.map( (frend: any, index: number) => {
            fetchPosts(id,frends.length, index,frend.frendId,day,month,year,false,-1)(dispatch);
        });
    });
    }
}

function fetchPostNameAndPic(myId: number ,posts: any, dispatch: any, reWriete: boolean, changePlace: number) {
    posts.map( (post: any, index: number) =>  {
     let newPost = fetch(`http://localhost:3000/user/${post.userId}`)
    .then( res => res.json())
    .then ( user =>{

     return   fetch(`http://localhost:3000/likes?colId=${post.id}`)
        .then( result => result.json())
        .then( likeList => {
          return  fetch(`http://localhost:3000/likes?colId=${post.id}&userId=${myId}`)
            .then( rez => rez.json())
            .then( liked => {
                    return post = {
                        ...post,
                        UserPic : user.userPic,
                        UserName : user.name,
                        likes: likeList.length,
                        liked: liked[0] !== undefined? 'fas fa-heart' : 'far fa-heart'
                   }
            })
        })
       
})
    newPost.then( data =>{dispatch( {
        type: "SET_POSTS",
        payload: data
    }) 
    if(index === posts.length-1 && reWriete === false){
        dispatch( {
            type: "SET_LAST_POSTS",
            payload: {day :data.postDay, month: data.postMonth, year:data.postYear, idLastPost: data.id}
        })
    }else if( reWriete === true){
        dispatch( {
            type: "CHANGE_LAST_POSTS",
            payload: {change: changePlace ,day :data.postDay, month: data.postMonth, year:data.postYear, idLastPost: data.id}
        })
    }
    })
})
}

export function fetchSearchUsers(name: string){
    return (dispatch: any) => {
        fetch(`http://localhost:3000/user?name_like=${name}`)
         .then( res => res.json())
         .then ( (users) => dispatch({
             type: "SET_SEARCH_RESULTS",
             payload: users
         }))
     }
}

export function likePost(id: number, liked: string, usrId: number) {
    return (dispatch: any) => {

        if(liked === 'fas fa-heart') {
            fetch(`http://localhost:3000/likes?colId=${id}&userId=${usrId}`)
            .then( res => res.json())
            .then(like => {
                fetch(`http://localhost:3000/likes/${like[0].id}`, {
                method: 'DELETE',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                })
              }).then( x => dispatch({type: 'REMOVE_LIKE', payload: id}));
            })
            
        }
        else {

            fetch('http://localhost:3000/likes', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                colId: id,
                userId: usrId
          })
        }).then( x => dispatch({type: 'ADD_LIKE', payload: id}));
        }
     }
}

export function registerUser(ime: string, sifra: string , mail: string) {

    return (dispatch: any) => {
        fetch('http://localhost:3000/user', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              name: ime,
              password: sifra,
              gmail: mail,
              userPic: ''
            })
          })
     }
}
