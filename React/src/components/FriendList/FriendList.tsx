import React from 'react';
import FriendListEl from './FriendListElement/FriendListEl';
import './FriendList.css';
interface Props {
    frends: any;
    itemClick: any;
    Route: boolean;
}
const friendList = (props: Props) => {
    
    let frendsList = props.frends.map(  (frend:any) =>
         (<FriendListEl sId={frend.id} id={frend.frendId} route={props.Route} ClickHandler={props.itemClick}name={frend.name} img={frend.userPic} key={frend.id} />)
        );
    
    return(
    <div className="friend-list">
        {frendsList}
    </div>
 );
}

export default friendList;