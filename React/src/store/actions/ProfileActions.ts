export function fetchProfileUser(id: number, month: number, year: number){
    return (dispatch: any) => {
        fetch(`http://localhost:3000/user/${id}`)
         .then( res => res.json())
         .then ( (user) => {
             dispatch({
             type: "FETCH_PROFILE_USER",
             payload: user
         });
         fetchProfilePosts(user,month,year)(dispatch);
         fetchProfilePic(user.id,dispatch);
        });
     }
}
function fetchProfilePic(id: number, dispatch:any ){
    fetch(`http://localhost:3000/posts?userId=${id}`)
         .then( res => res.json())
         .then ( (posts) => {
             
            dispatch({
             type: "SET_PROFILE_PICTURES",
             payload: posts
         });
         
         })
}
export function fetchProfilePosts(user: any, month: number, year: number){
    return (dispatch: any) => {
        fetch(`http://localhost:3000/posts?userId=${user.id}&postMonth=${month}&postYear=${year}&_sort=id&_order=desc`)
         .then( res => res.json())
         .then ( (posts) => {
             if(posts[0] === undefined){
                if(month > 0){
                    fetchProfilePosts(user,month-1,year)(dispatch);
                }else {
                    if(year > 2018)
                    fetchProfilePosts(user,12,year-1)(dispatch);
                }
             }else{
                 posts.map( (post: any) => {
                    const modPosts = {
                        ...post ,
                         UserName: user.name,
                         UserPic: user.userPic,
                         liked: 'far fa-heart'
                    };
                    dispatch({
                        type: "SET_PROFILE_POST",
                        payload: modPosts
                    })
                 })
             }
         });
           
    }
}
export function addFrend(mId: number, fId: number){
    return (dispatch: any) => {
        fetch('http://localhost:3000/frends', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            myId: mId,
            frendId: fId
          })
        })

     }
}

export function saveUserInfo(user: any, phone: string, job: string, rel: string, picUrl: string) {
    return (dispatch: any) => {
        fetch(`http://localhost:3000/user/${user.id}`, {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            ...user,
            phone: phone,
            job: job,
            relationship: rel,
            userPic: picUrl
          })
        })

     }
}

export function makePost(tekst: string, slika: string, id: number ) {
  return (dispatch: any) => {
    if( tekst !== '') {
      let Today = new Date();
      fetch('http://localhost:3000/posts', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          postTxt: tekst,
          userId: id,
          picUrl: slika,
          postDay: Today.getDate(),
          postMonth: Today.getMonth(),
          postYear: Today.getFullYear() 
        })
      });

    }
 }
}