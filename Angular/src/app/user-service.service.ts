import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, interval } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromStore from './store';
@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  loginUser$: Observable<User>;
  searching = false;
  userName: string;
  userPic: string;
  check: any = null;
  url = 'http://localhost:3000';
  httpHeaders = new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  });
  options = {
    headers: this.httpHeaders
  };
  constructor(private http: HttpClient, private store: Store<fromStore.CBState>) {

  }

  getUser(index: number, fax: number): Observable<User> {

    return this.http.get<User>(`${this.url}/user?id=${index}&Fax=${fax}`, this.options);

  }
  async loginUser(index: number, fax: number) {

    this.check = await this.getUser(index, fax);
    return new Promise((resolve, reject) => {
      resolve(this.check);
    });

  }
  async registerUser(newUser: any) {
    this.check = await this.getUser(newUser.id , newUser.Fax);
    // tslint:disable-next-line: radix
    if (this.check.Index === newUser.index && this.check.Fax === parseInt(newUser.fax)) {
      return new Promise<boolean>(resolve => resolve(false));
    } else {

      this.http.post<User>(`${this.url}/user`, newUser, this.options).subscribe();
      return new Promise<boolean>(resolve => resolve(true));
    }
  }
  updateUser(newUser: User) {

    this.http.put<User>(`${this.url}/user/${newUser.id}`, newUser, this.options).subscribe();
  }
  searchName(name: string): Observable<User[]> {

    return this.http.get<User[]>(`${this.url}/user?Name_like=${name}`, this.options);
  }

}
