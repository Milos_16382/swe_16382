import React from 'react';
import './Card.css';
import Avatar from '../Avatar/Avatar';

interface Props {
   post: any;
   Like: any;
   user: any;
}
const card = (props: Props) =>{ 

    return(
    <div className="card">
        <div className="card-header">
            <Avatar size="small" link={props.post.UserPic}/>
            <p className="user-name">{props.post.userName}</p>
        </div>
        <div className="card-content">
        <div className="line"></div>
            <img src={props.post.picUrl}/>
            <p className="card-text">{props.post.postTxt}</p>
        <div className="line"></div>
        <div className="like">
            <i className={`${props.post.liked}`}   onClick={() => props.Like(props.post.id, props.post.liked, props.user.id)}>
            <p > { props.post.likes} </p>
            </i>
        </div>
        </div>
    </div>
);}

export default card;