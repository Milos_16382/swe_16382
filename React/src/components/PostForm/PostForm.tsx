import React from 'react';
import './PostForm.css'
interface Props {
    PostTextHandler: any;
    PostHandler: any;
    PostUrlHandler: any;
}
const postForm = (props: Props) => (
    
    <div className="post-form">
        <textarea onChange={props.PostTextHandler}></textarea>
        <div className="pic-btn">
        <button onClick={props.PostHandler}>POST</button>
        <i className="far fa-images"></i>
        <input type="text" placeholder="url" onChange={props.PostUrlHandler}></input>
        </div>
    </div>

);

export default postForm;