import * as fromUser from '../actions/user.action';
import { Subject } from 'rxjs';
export interface UserState {
    user: User;
    loaded: boolean;
    loading: Subject<boolean>;
    searching: boolean;
    searchUser: User;
    loginUser: User;
    searchUsers: User[];
}

export const initialState: UserState = {
    user: {
        Name: 'miske',
        Password: '',
        Fax: 0,
        id: 0,
        PicUrl: ''
    },
    loaded: false,
    loading: new Subject(),
    searching: false,
    searchUser: {
        Name: '',
        Password: '',
        Fax: 0,
        id: 0,
        PicUrl: ''
    },
    loginUser: {
        Name: '',
        Password: '',
        Fax: 0,
        id: 0,
        PicUrl: ''
    },
    searchUsers: []
};

export function reducer(state = initialState, action: fromUser.UserAction): UserState {

    switch (action.type) {
        case fromUser.GET_USER: {
            state.loading.next(true);
            return {
                ...state,
                loaded: false
            };
        }
        case fromUser.GET_USER_SUCCESS: {
            const user = action.payload[0];
            state.loading.next(false);
            return {
                ...state,
                loaded: true,
                user,
            };
        }
        case fromUser.SET_SEARCHING: {
            const searching = action.payload;
            return {
                ...state,
                searching
            };
        }
        case fromUser.SEARCH_USER_NAME_SUCCESS: {
            const searchUsers = action.payload;
            return {
                ...state,
                searchUsers
            };
        }
        case fromUser.SET_SEARCH_USER: {
            const searchUser = action.payload;
            return {
                ...state,
                searchUser
            };
        }
        default : {
            return state;
        }
    }
}

export const getUser = (state: UserState) => state.user;
export const getUserLoaded = (state: UserState) => state.loaded;
export const getUserLoading = (state: UserState) => state.loading;
export const getSearching = (state: UserState) => state.searching;
export const getLogin = (state: UserState) => state.loginUser;
export const getSearchUsers = (state: UserState) => state.searchUsers;
export const getSearchUser = (state: UserState) => state.searchUser;
