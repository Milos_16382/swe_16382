import { getEmptyEl, getMyQuestions, getUser } from "../Profesor/Handlers";
import { fromEvent , from,zip} from "rxjs";
import { pluck, debounceTime, distinctUntilChanged, filter, mergeMap, take, map , switchMap, tap} from "rxjs/operators";
import { fillElement, fillHtmlElement, fetchPostCall } from "../lib";

const url = 'http://localhost:3000';

function getPastQuestions(question) {
    return from(
      fetch(
        `${url}/pitanja?question_like=${question}&_sort=date,time&_order=desc,desc&_limit=30`
      ).then(response => response.json())
    );
}

export function guestSearchHandler() {
    
  
    const textBox = document.querySelector(".search");
  
    let data = fromEvent(textBox, "input")
      .pipe(
        pluck("target", "value"),
        debounceTime(500),
        distinctUntilChanged(),
        filter(text => text.length >= 3),
        tap(x => {
          const conta = document.getElementsByClassName("background")[0];
          while (conta.firstChild) {
            conta.removeChild(conta.firstChild);
          }
          return x;
        }),
        map( search => search[0] === 'n' ? {type: 'n', search: search.substring(2)} 
        : {type: 'q', search: search} ),
        switchMap(sObj =>  sObj.type === "n" ? getMyQuestions(sObj.search): getPastQuestions(sObj.search)),
        //switchMap(array => from(array)),
        tap(element => 
            element.forEach( el => {
              zip(getUser(el.userName),getEmptyEl()).pipe(
                take(1),
                tap(zipObj => fillHtmlElement(zipObj[1],  el, zipObj[0][0].name, handelClick))
              ).subscribe();

            })
        ),
        // mergeMap(el =>
        //   getEmptyEl().pipe(
        //     take(1),
        //     tap(html =>fillHtmlElement(html,  el, user.name, handelClick))
        //   )
        // )
      )
      .subscribe();

  }

  export function handelClick(div, data) {
    div.onclick = event => {
      document.getElementsByClassName("backdrop")[0].className = "backdrop";
  
      const quest = document.getElementsByClassName("question-modal")[0];
      const modal = document.getElementsByClassName("modal")[0];
      const odgovori = document.getElementsByClassName("a-container")[0];
  
      while (odgovori.firstChild) {
        odgovori.removeChild(odgovori.firstChild);
      }
      data.answers.map((answer, index) => {
        let option = document.createElement("p");
        setAnswerClickHandler(option, index, data.id);
        option.innerHTML = answer;
        odgovori.appendChild(option);
      });
      quest.innerHTML = data.question;
      modal.className = "modal";
    };
  }

  function setAnswerClickHandler(answ, index, id) {
    answ.onclick = event => {
      fetch(`${url}/odgovori?qId=${id}`)
        .then(response => response.json())
        .then(odg => {
          let newRes = [...odg[0].resoults];
          newRes[index]++;
          fetch(`${url}/odgovori/${odg[0].id}`, {
            method: "PUT",
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify({...odg[0],resoults: newRes})
          }).then(res => res.json())
        .then(x => console.log(x));
        });
    };
  }