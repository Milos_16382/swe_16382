import { Component, OnInit } from '@angular/core';
import { UserServiceService } from 'src/app/user-service.service';
import { Router } from '@angular/router';
import {  Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  resoults$: Observable<User[]>;
  sUser$: Observable<User>;
  searchText: string;
  searchFax: number;
  searching = true;
  constructor(private userService: UserServiceService, private router: Router,
              private nav: NavController, private store: Store<fromStore.CBState>) {
   }
    ngOnInit() {
      this.resoults$ = this.store.select(fromStore.getSUsers);
      this.sUser$ = this.store.select(fromStore.getSUser);
      setTimeout(() => this.userService.searching = true);

  }
  // searchOn(): Promise<boolean>{
  //   //setTimeout( x => this.userService.searching = true,);
  //   return new Promise( resolve => {
  //     resolve(true);
  //   })
  // }
  openUser(sFax: number, sIndex: number, sPic: string, name: string) {
   // this.userService.user = this.userService.searchUser$;
   this.store.dispatch(new fromStore.SetSearchUser({
    Name: name,
    id: sIndex,
    Password: '',
    Fax: sFax,
    PicUrl: sPic
  }));
   setTimeout( () => this.searching = false, 200);
    // this.nav.navigateForward('/home');
    // this.router.navigate(['/home/user']);
  }
  clearSearch() {
    this.searching = true;
    this.searchText = '';
  }
  search() {
    this.store.dispatch(new fromStore.SearchUserName(this.searchText));
    this.searching = true;
  }
  searchOff() {
    setTimeout(() => this.userService.searching = false);
  }
}
