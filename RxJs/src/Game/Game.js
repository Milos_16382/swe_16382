import { Obsticle } from './Obsticle'
import { Observable, fromEvent, Subject ,merge, timer} from 'rxjs';
import{take,filter,map,takeUntil, debounceTime, delay, tap, skipUntil} from "rxjs/operators";

const body = document.querySelector(".main");
const gameOver$ = fromEvent(body, 'mouseover');
const stop$ = new Subject();
stop$.subscribe(x => console.log('emitovao'));
let obsCreate = Observable.create( obs => {

    setInterval( () =>  {
        const o1 = new Obsticle();
        //body.appendChild(o1.obsticle);
        setTimeout( () => o1.obsticle.className = 'obsticle big', 500);
        let objEvent$ = fromEvent(o1.obsticle,'mouseover');
        merge( gameOver$, objEvent$ ).pipe(
            skipUntil(timer(2000)),
            takeUntil(stop$),
            tap( x => stop$.next())
        ).subscribe()

        obs.next(o1.obsticle)}, 1000);

    
});


export function initGame(){

    obsCreate.pipe(
        takeUntil(stop$),
        map(x => body.appendChild(x)),
        debounceTime(500),
        map( obst => {
            obst.className = 'obsticle big';
            return obst;
           }),
        delay(4050),
        map( obs => body.removeChild(obs)),
       
    ).subscribe();
}

