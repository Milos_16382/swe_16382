import React from 'react';
import './PostsList.css';
import Card from '../Card/Card';

interface Props{
    posts: any;
    loadPosts: any;
    LikeHandler: any;
    Me: any;
}
const postsList = (props: Props) =>{ 
    let posts = props.posts.map(  (post:any) =>
         (<Card post={post}  key={post.id} Like={props.LikeHandler} user={props.Me} />)
        );

    return (
    <div className="post-list">
        {posts}
        <p className="load" onClick={() => props.posts[0] === undefined ? props.loadPosts(0,0,0) : props.loadPosts(props.posts[props.posts.length-1].postDay,props.posts[props.posts.length-1].postMonth,props.posts[props.posts.length-1].postYear)}>Load More Posts</p>
    </div>
);
}
 
export default postsList;