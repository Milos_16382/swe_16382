import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { UserServiceService } from '../user-service.service';
import { setTNodeAndViewData } from '@angular/core/src/render3/state';
import { Observable, from } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromStore from '../store';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss'],
})
export class UserDataComponent implements OnInit {
  user$: Observable<User>;
  namePlace: Promise<string>;
  urlPlace: Promise<string>;
  name: string='';
  pass: string;
  url: string='';
  constructor(private popoverController: PopoverController, private userService: UserServiceService,
              private store: Store<fromStore.CBState>) {
    this.user$ = this.store.select(fromStore.getUser);
   }

  ngOnInit() { }

  newUrl(ev) {
    this.url = ev.target.value;
  }

  newName(ev) {
    this.name = ev.target.value;
  }

  newPassword(ev)  {
    this.pass = ev.target.value;
  }

  async change() {

    let index: number;
    let fax: number;

    this.user$.subscribe( user => {
      index = user.id;
      fax = user.Fax;
    });
    if (this.name === '') {
      this.name = await this.namePlace;
    }
    if (this.url === '') {
      this.url = await this.urlPlace;
    }
    this.store.dispatch(new fromStore.UpdateUser({
      id: index,
      Fax: fax,
      Name: this.name,
      Password: this.pass,
      PicUrl: this.url
    }));
    this.popoverController.dismiss();
  }

  cancle() {
    this.popoverController.dismiss();
  }
  // async setName() {
  //   this.name = await this.namePlace;
  // }
  // async setUrl() {
  //   this.url = await this.urlPlace;
  // }
}
