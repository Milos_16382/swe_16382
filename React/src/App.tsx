import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';

import Home from './pages/Home/Home.page';
import Register from './pages/Register/Register.page';
import Login from './pages/Login/Login.page';
import Profile from './pages/Profile/Profile.page';
class App extends Component{

  render(){

  return (
    <div className="App">
      <Router>
        <div>
        <Route exact path={"/"} component={Login}/>
        <Route  path={"/register"} component={Register}/>
        <Route  path={"/home/:id"} component={Profile}/>
        <Route  exact path={"/home"} component={Home}></Route>
        </div>
      </Router>
    </div>
  );
  }
}

export default App;
