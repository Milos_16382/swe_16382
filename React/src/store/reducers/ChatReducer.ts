
interface User{
    id: number;
    name: string;
    password : string;
    gmail : string;
    userPic: string;

}
interface IChatState {
    sendCompleete: boolean;
    chatUser: User;
    chatHide: string;
    chatClose: string;
    chat: any;
    message: string;
}

const initialChatState: IChatState =  {

        chatUser: {
            id: 0,
            name: "",
            password : "",
            gmail : "",
            userPic: ""
        },
        chatHide: 'closed',
        chatClose: 'close',
        chat: [],
        sendCompleete: false,
        message: '',
} 

const chatReducer = (state = initialChatState , action: any) => {
    switch(action.type){
        case "SET_CHAT_USER":
            state = {
                ...state,
                chatUser: action.payload
            }
        break;
        case 'SET_CHAT_CLOSED':
            let close;
            if(state.chatClose === ''){
                close = 'close';
            } else {
                close = '';
            }
            state = {
                ...state,
                chatClose: close,
                chatHide: 'closed'
            }
    break;
    case 'SET_CHAT_HIDE':
            let hide;
            if(state.chatHide === ''){
                hide = 'closed';
            } else {
                hide = '';
            }
            state = {
                ...state,
                chatHide: hide
            }
    break;
    case 'OPEN_CHAT':
            state = {
                ...state,
                chatHide: '',
                chatClose: ''
            }
    break;
    case 'SET_CHAT':
            state = {
                ...state,
                chat: action.payload,
            }
    break;
    case 'APPEND_CHAT':
        if(action.payload.length === undefined){
            state = {
                ...state,
                chat: [action.payload,...state.chat]
            }
        }else if(action.payload.length !== 0) {
            state = {
                ...state,
                chat: [action.payload,...state.chat]
            }
        }
    break;
    case 'SEND_COMPLETE':
            state = {
                ...state,
                sendCompleete: true
            }
    break;
    case 'SEND_COMPLETE_RESET':
            state = {
                ...state,
                sendCompleete: false
            }
    break;
    case 'SET_MSG':
            state = {
                ...state,
                message: action.payload
            }
    break;
    case 'CLEAR_MSG':
            state = {
                ...state,
                message: ''
            }
    break;

    }
    return state;
}

export default chatReducer;