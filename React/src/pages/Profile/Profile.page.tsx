import React,{ Component } from "react";
import { Link, Route} from "react-router-dom";
import './Profile.page.css';

import Profile from '../../components/Profile/Profile';
import Galery from '../../components/Galery/Galery';
import { connect } from "react-redux";
import { fetchProfileUser, fetchProfilePosts, addFrend, saveUserInfo } from "../../store/actions/ProfileActions";
import PostForm from "../../components/PostForm/PostForm";
import Toolbar from "../../components/Toolbar/Toolbar";
import InfoCard from "../../components/InfoCard/InfoCard";
import PostsList from "../../components/PostsList/PostsList";
import { likePost } from "../../store/actions/UserActions";


interface Props {
    children: any;
    match:any;
    user: any;
    profile: any;
    fetchUser: any;
    pictures: any;
    posts: any;
    clearProfile: any;
    frends: any;
    updatePosts: any;
    addFrend: any;
    saveInfo: any;
    PostLikeHandler: any;
}
class ProfilePage extends Component<Props> {

    constructor(props: Props){
        super(props);
    }
    componentDidMount() {
        this.props.clearProfile();
        let Today = new Date();
        this.props.fetchUser(this.props.match.params.id, Today.getMonth() , Today.getFullYear());
    }
    PostUpdateHandler = ( day: number, month: number, year: number) => {
            console.log({id: this.props.profile.id, name: this.props.profile.name ,userPic:this.props.profile.UserPic}, "profil" , month ,"opremm", year);
           this.props.updatePosts(this.props.profile,month-1,year);
      }
    render() {

        return (
            <div>
                <header className="toolbar">
        <nav className="toolbar_navigation">
                <Link to={`/home`}><i className="fas fa-arrow-left"></i></Link>
            </nav>
        </header>
                <div style={{height: "54px", width: "100%" }} />
                <Profile pic={this.props.profile.userPic}  name={this.props.profile.name} />
                <div className="add-frend" onClick={() => this.props.addFrend(this.props.user.id,this.props.profile.id)}> <p > Add Frend</p> </div>
                <div className="profile-liks"> 
                    <div className="spacer"/>
                    <div>
                        <Link to={`${this.props.match.url}/posts`}><i className="far fa-clone"></i></Link>
                    </div>                    
                    <div className="spacer"/>
                    <div className="line" />

                    <div className="spacer"/>
                    <div>
                        <Link to={`${this.props.match.url}/photos`}><i className="far fa-images"></i></Link>
                    </div>                    
                    <div className="spacer"/>
                    <div className="line" />

                    <div className="spacer"/>
                    <div>
                        <Link to={`${this.props.match.url}/information`}><i className="fas fa-info"></i></Link>
                    </div>                    
                    <div className="spacer"/>
                </div>
                 <Route path={"/home/:gmail/posts"} render={(props) => <PostsList Me={{}} LikeHandler={this.props.PostLikeHandler} loadPosts={this.PostUpdateHandler} posts={this.props.posts[0] === undefined ? [] : this.props.posts} />}></Route>
                <Route path={"/home/:id/photos"} render={(props) => <Galery photos={this.props.pictures[0] === undefined ? [] : this.props.pictures} />}></Route>
                <Route path={"/home/:id/information"} render={(props: any) => 
                <InfoCard user={this.props.profile} 
                me={ this.props.profile.id === this.props.user.id ? true : false} 
                save = {this.props.saveInfo} ></InfoCard>}>
                </Route>
            </div>
        );
    };
}


const mapStateToProps = (state: any) => {
  return {
    user: state.userReducer.user,
    profile: state.profileReducer.userProfile,
    pictures: state.profileReducer.profilePictures,
    posts: state.profileReducer.profilePosts,
    frends: state.userReducer.frends
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
      fetchUser: (id: number, month: number, year: number) => {
        dispatch(fetchProfileUser(id, month, year));
      },
      clearProfile: () => {
        dispatch({type: "CLEAR_PROFILE"});
      },
      updatePosts: (user:any, month: number, year: number) => {
        dispatch(fetchProfilePosts(user , month, year));
      },
      addFrend: (mId: number, fId: number) => {
          dispatch(addFrend(mId,fId));
      },
      PostLikeHandler : (id: number, liked: string, userId: number) => {
        dispatch(likePost(id,liked,userId));
      },
      saveInfo: (user: any,cel: string, job: string, rele: string, pic: string) => {
          dispatch(saveUserInfo(user,cel,job,rele,pic))
      }
  };
};

 export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);

