import React from 'react';
import './Profile.css';

import Avatar from '../Avatar/Avatar';
interface Props {
    name: string;
    pic: string;
}
const profile = (props: Props) => (
    <div className="profile">
            <Avatar size="big" link={props.pic}/>
            <div className="line"></div>
            <strong>{props.name}</strong>
    </div>
);


export default profile;