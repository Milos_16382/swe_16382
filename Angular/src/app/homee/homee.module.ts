import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AppModule} from '../app.module'

import { LoginPage } from '../login/login.page';
import { SearchPage } from '../home/search/search.page';
import { HomeeComponent } from './homee.component';

const routes: Routes = [
    // {
    //   path: 'login',
    //   component: LoginPage
    // },
    // {
    //   path: 'search',
    //   component: 
    // },
  ];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
      ],
  declarations: [HomeeComponent],
  exports: [HomeeComponent],
})
export class HomeeModule {}
