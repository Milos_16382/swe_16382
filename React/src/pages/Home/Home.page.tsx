import React, { Component } from "react";
import './Home.page.css';
import {connect} from 'react-redux';
import { fetchUser , fetchFrends, fetchSearchUsers, fetchPosts, likePost } from '../../store/actions/UserActions';
import { fetchChatUser, fetchChat } from '../../store/actions/ChatActions';
import Toolbar from '../../components/Toolbar/Toolbar'
import SideDrawer from '../../components/SideDrawer/SideDrawer';
import Backdrop from '../../components/BackDrop/Backdrop';

import ChatBox from '../../components/Chat/ChatBox/ChatBox';
import PostForm from '../../components/PostForm/PostForm';
import PostsList from "../../components/PostsList/PostsList";
import { makePost } from "../../store/actions/ProfileActions";


interface Props{
  fetchUser: any;
  fetchChatUser: any;
  fetchFrends: any;
  fetchResoults: any;
  getChat: any
  openChat: any;
  user: any;
  posts: any ;
  lastPost: any;
  loadingFrends: boolean;
  frends: any;
  resoults: any;
  drawerShow:boolean ;
  searchShow:boolean ;
  resoultsShow:boolean ;
  postsUpdated: boolean ;
  drawerToggleClickHandler: any;
  showSearchClickHandler: any;
  showResoultsClickHandler: any;
  backDropClickHandler: any;
  updatePosts: any;
  setUpdated: any;
  unSetUpdated: any;
  PostLikeHandler: any;
  PostHandler: any;
}
class Home extends Component<Props>{

    state = {
        sideDrawerOpen: false,
        searchShow: false,
        resoultsShow: false,
      };

      constructor(props : Props) {
        super(props);
        if(this.props.posts[0] === undefined){
          let Today = new Date();
          if(this.props.frends[0] === undefined)
          this.props.fetchFrends(this.props.user.id,Today.getDate(),Today.getUTCMonth(),Today.getFullYear());
        }
      }
      componentDidMount() {
       

        let Today = new Date();
        console.log(Today.toDateString());
      }
      show = true;
      searchInputHandler = (event: any) => {

        this.props.fetchResoults(event.target.value);
        if( this.show ){
          this.props.showResoultsClickHandler();
        }
        console.log(this.show, "show");
        if(event.target.value === ''){
          this.props.showResoultsClickHandler();
          this.show = true;
        }else {
          this.show = false;
        }
      }
     
      frendListClickHandler = (id: number) => {
        this.props.openChat();
        this.props.fetchChatUser(id, this.props.user.id);
        this.props.getChat(this.props.user.id,id)
      }
      postInput = '';
      postUrl = '';
      PostInputHandler = (event: any) => {
        this.postInput = event.target.value;
      }
      PostUrlInputHandler = (event: any) => {
        this.postUrl = event.target.value;
      }
      PostHandler = () => {
        this.props.PostHandler(this.postInput, this.postUrl, this.props.user.id);
      }
      PostUpdateHandler = ( day: number, month: number, year: number) => {
        this.props.unSetUpdated();
        if(this.props.frends !== undefined){
          this.props.frends.map( (frend: any, index: number) => this.props.updatePosts(
             this.props.user.id ,
             this.props.frends.length-1,
             index,frend.id,
             0,
             0,
             0,
            //  this.props.lastPost[index].day,
            //  this.props.lastPost[index].month,
            //  this.props.lastPost[index].year,
             true,
             this.props.lastPost[index] === undefined ? 0 : this.props.lastPost[index].idLastPost
             ));
        }
      }
      
    
    render(){
        let backdrop;
        if (this.props.drawerShow) {
            backdrop = <Backdrop click={this.props.backDropClickHandler}/>
        }
        
        if (this.props.postsUpdated) {
          //this.props.frends.map( (frend: any, index: number) => this.props.updatePosts(this.props.frends.length-1, index,frend.id,this.props.lastPost[index].day,this.props.lastPost[index].month,this.props.lastPost[index].year));
        }
        
        return (
        <div >
          
          <Toolbar resoults={this.props.resoults} resoultsClass={this.props.resoultsShow} elClick={this.props.showSearchClickHandler}
          showResoults={this.searchInputHandler}  showSearch={this.props.showSearchClickHandler}  
          show={this.props.searchShow} drawerClickHandler={this.props.drawerToggleClickHandler} />
          <SideDrawer frendClickHandler={this.frendListClickHandler} show={this.props.drawerShow} user={this.props.user} frends={this.props.frends}/>
            {backdrop}
            <main className="main">
            <div className="spacer" />
            <PostForm PostHandler={this.PostHandler} PostTextHandler={this.PostInputHandler} PostUrlHandler={this.PostUrlInputHandler}/>
            <PostsList Me={this.props.user} LikeHandler={this.props.PostLikeHandler} posts={this.props.posts} loadPosts={this.PostUpdateHandler}/>
            </main>
            <ChatBox />
      </div>
        )
    }
}

const mapStateToProps = (state: any) => {
  return {
    user: state.userReducer.user,
    posts: state.userReducer.posts,
    lastPost: state.userReducer.lastPost,
    frends: state.userReducer.frends,
    resoults: state.userReducer.resoults,
    drawerShow: state.userReducer.sideDrawerOpen,
    searchShow: state.userReducer.searchShow,
    resoultsShow: state.userReducer.resoultsShow,
    postsUpdated: state.userReducer.postsUpdated
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchUser: (gmail: string) => {
      dispatch(fetchUser(gmail));
    },
    fetchChatUser: (id: number) => {
      dispatch(fetchChatUser(id));
    },
    fetchFrends: (id: number, day: number, month: number, year: number) => {
      dispatch(fetchFrends(id,day,month,year));
    },
    fetchResoults: (name: string) => {
      dispatch(fetchSearchUsers(name));
    },
    openChat: () => {
      dispatch({type:"OPEN_CHAT"});
    },
    getChat: (myID: number, frendID: number) => {
      dispatch(fetchChat(myID,frendID));
    },
    PostHandler: (txt: string, url: string, id: number) => {
      dispatch(makePost(txt,url,id));
    },
    drawerToggleClickHandler:  () => {
      dispatch({type: "SET_SIDE_DRAWER_OPEN"});
    },
    showSearchClickHandler:  () => {
      dispatch({type: "SET_SEARCH_SHOW"});
    },
    showResoultsClickHandler:  () => {
      dispatch({type: "SET_RESOULTS_SHOW"});
    },
    backDropClickHandler:  () => {
      dispatch({type: "SET_SIDE_DRAWER_CLOSE"});
    },
    updatePosts: (idMoj: number, length: number, index: number ,id:number,day: number,
       month: number, year: number, write: boolean, lastID: number) => {
      dispatch(fetchPosts(idMoj,length,index,id ,day, month, year, write,lastID));
    },
    PostLikeHandler : (id: number, liked: string, userId: number) => {
      dispatch(likePost(id,liked,userId));
    },
    unSetUpdated: () => {
      dispatch({type: "UN_SET_POST_UPDATE"});
    },
    setUpdated: () => {
      dispatch({type: "SET_POST_UPDATE"});
    }
  };
};

 export default connect(mapStateToProps, mapDispatchToProps)(Home);
