import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';
import { HomeeComponent } from '../homee/homee.component';
import { AppModule } from '../app.module';
import { HomeeModule } from '../homee/homee.module';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    HomeeModule,
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
