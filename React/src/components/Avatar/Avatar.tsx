import React from 'react';
import './Avatar.css'

interface Props {
    link: any;
    size: string;
}

const avatar = (props: Props) => (
    <div className={props.size}>
        <img src={props.link}/>
    </div>
);

export default avatar;