import { Component, OnInit, Input, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { ColectionServiceService } from '../colection-service.service';
import { ToastController, AlertController, PopoverController } from '@ionic/angular';
import { UserServiceService } from '../user-service.service';
import { resolve } from 'url';
import { Observable } from 'rxjs';
import { UserDataComponent } from '../user-data/user-data.component';
import { async } from '@angular/core/testing';
import { EditColComponent } from '../edit-col/edit-col.component';
import { Store } from '@ngrx/store';
import * as fromStore from '../store';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  colection$: Observable<Colection[]>;
  user$: Observable<User>;

  like: 'heart';
  constructor(private colectionService: ColectionServiceService, private toastCtrl: ToastController,
              private userService: UserServiceService,
              private alertController: AlertController, private popoverController: PopoverController,
              private store: Store<fromStore.CBState>) {


  }
  async ngOnInit() {
    // this.colection = this.colectionService.getColection();
    this.colection$ = this.store.select(fromStore.getColections);
    this.user$ = this.store.select(fromStore.getUser);
    // this.store.dispatch( new fromStore.GetColections());

    // this.store.dispatch( new fromStore.GetUser());
    // this.store.dispatch( new fromStore.GetUser());
    console.log(this.colection$);
    console.log('user', this.user$);
    // this.colection.forEach(element => this.colNmb = new Promise(resolve => {
    //   resolve(element.length.toString());
    // }))


    // if (this.userService.searching === false) {
    //   this.userName.then(name => this.userService.userName = name);
    //   this.userPic.then(pic => this.userService.userPic = pic);
    // }

    // this.userFax = this.getFax();
    this.getColLength();
    // setTimeout(() => this.userService.searching = false);
    // this.searchOn(null).then(res => {
    //   console.log("nakon klika", this.userService.searching);
    //     this.userService.searching = res;
    //     console.log("a sad", this.userService.searching);
    // })
    // }
    // console.log(this.userService.searchUser);

    // if (this.userService.searchUser !== undefined) {
    //   this.user = this.userService.searchUser;
    // }
    // USER SERVICE AND CHECK FOR SEARCH USER
  }

  async loginToast() {
    // let col = parseInt(await this.colNmb);
    // if (col === 0) {
    //   const toast = await this.toastCtrl.create({
    //     message: 'Click the plus sign to add your first collection',
    //     duration: 7000,
    //     position: 'bottom'
    //   });
    //   toast.present();
    // }
  }
  delColection(id: number) {

    this.deleteAlert(id);


  }
  doRefresh(event) {

    // this.colection$ = this.colectionService.getColection();
    event.target.complete();
  }
  likeCol(id: number) {
    // this.colectionService.like(id);
    // setTimeout(() => this.colection$ = this.colectionService.getColection(), 200);
  }
  retUser() {
    // this.userService.searchUser$ = this.userService.user; za search se koristi !!!
  }
  getColLength() {

    // this.colection$.subscribe(col => col.length === 0 ? this.loginToast() : 0)
  }
  async deleteAlert(i: number) {
    const alert = await this.alertController.create({
      header: 'Are you sure!',
      message: '<strong>you want to delete this colection</strong>!!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary'

        }, {
          text: 'Yes',
          handler: () => {
            this.colectionService.deleteColection(i);
            // setTimeout(() => this.colection$ = this.colectionService.getColection(), 500);
          }
        }
      ]
    });

    await alert.present();
  }
  makeChange() {
    if (this.userService.searching === false) {
      this.presentPopover();
    }
  }

  async presentPopover() {
    // const popover = await this.popoverController.create({
    //   component: UserDataComponent,
    //   componentProps: {
    //     namePlace: this.userName,
    //     pass: this.userService.searchUser.password,
    //     urlPlace: this.userPic
    //   },
    //   translucent: true
    // });
    // return await popover.present();
  }
  async editColection(id: number, name: string, vis: number) {
    const popover = await this.popoverController.create({
      component: EditColComponent,
      componentProps: {
        colectionId: id
      },
      translucent: true
    });
    return await popover.present();
  }
  getFax() {
    // return this.userService.getUser().then(x => {
    //   return new Promise<string>(resolve => {

    //     switch (x.Fax) {
    //       case 1:
    //         resolve('Elfak');
    //         break;
    //       case 2:
    //         resolve('FF');
    //         break;
    //       case 3:
    //         resolve('DIF');
    //         break;
    //     }
    //   });
    // });
  }
  searchCheck(): boolean {
    let check: boolean;
    this.store.select(fromStore.getSearching).subscribe(search => {
      if (search === false) {
        check = true;
      } else {
        check = false;
      }
    });
    return check;
  }
}

