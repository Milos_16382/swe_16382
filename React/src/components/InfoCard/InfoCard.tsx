import React, { Component } from 'react';
import './InfoCard.css'

interface Props{
    user: any;
    me: boolean;
    save: any;
}
class infoCard extends Component<Props>{ 
    
    state = {
        cel : "",
        job : "",
        relationship : "",
        pic: ""
    }
    display: any;
    
    componentDidMount() {
        this.setState({
            cel : this.props.user.phone,
            job : this.props.user.job,
            relationship : this.props.user.relationship,
            pic : this.props.user.userPic
        })
    }

    render() {
        if(this.props.me === true) {

            this.display = (
                    <div className="info">
                    <div className="container">
                    <label > Cel Phone:</label> <input value={this.state.cel} onChange={(event) => this.setState({cel : event.target.value})}></input>
                    </div> 
                    <div className="container">
                    <label > Job:</label> <input value={this.state.job} onChange={(event) => this.setState({job : event.target.value}) }></input>
                    </div> 
                    <div className="container">
                    <label > Relationship:</label> <input value={this.state.relationship} onChange={(event) =>this.setState({relationship : event.target.value})}></input>
                    </div> 
                    <div className="container">
                    <label > Profile Pic:</label> <input value={this.state.pic} onChange={(event) =>this.setState({pic : event.target.value})}></input>
                    </div> 
                    <button onClick={() => this.props.save(this.props.user,this.state.cel,this.state.job,this.state.relationship,this.state.pic)}>Save</button>
                    </div>
                    
                    );
            }else{
        
            this.display = (
                 <div className="info">
                    <div className="container">
                    <label > Cel Phone:</label> <p >{this.props.user.phone}</p>
                    </div> 
                    <div className="container">
                    <label > Job:</label> <p >{this.props.user.job}</p>
                    </div> 
                    <div className="container">
                    <label > Relationship:</label> <p>{this.props.user.relationship}</p>
                    </div> 
                </div>
                    )
            }
        return(
        <div className="info-card">
            {this.display}        
        </div>
    );
    }
}
export default infoCard;