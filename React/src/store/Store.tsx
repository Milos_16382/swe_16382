import { createStore, applyMiddleware, combineReducers } from 'redux';
import userReducer from './reducers/UserReducer';
import chatReducer from './reducers/ChatReducer';
import profileReducer from './reducers/ProfileReducer';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { any } from 'prop-types';

const rootReducer = combineReducers({userReducer,chatReducer, profileReducer})
export default createStore(rootReducer, applyMiddleware(thunk, logger));

interface User{
    id: number;
    name: string;
    password : string;
    gmail : string;
    userPic: string;
}
interface Ichat {
    chatUser: User;
    chatHide: string;
    chatClose: string;
    chat: [];
}