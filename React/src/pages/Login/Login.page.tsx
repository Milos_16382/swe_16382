import React,{ Component } from "react";
import './Login.page.css';
import { Route } from "react-router";

import Input from '../../components/Input/Input';
import { connect } from "react-redux";
import { fetchUser } from "../../store/actions/UserActions";


interface LoginProps{
    fetchUser: any;
    user: any;
    loading: boolean;
    gmail: string;
    pass: string;
    setGmail: any;
    setPass: any;
}

class Login extends Component<LoginProps> {

    
    gmailHandler = (event : any) =>{
        this.props.setGmail(event.target.value);
    }
    passHandler = (event : any) =>{
        this.props.setPass(event.target.value);
    }
    //miske@gmail.com
    loginHandler = () => {
        this.props.fetchUser(this.props.gmail);
    }
    render() {


        return (
            <div className="login-page">
                <img src="https://wallpapermemory.com/uploads/654/purple-wallpaper-hd-1920x1080-405334.jpg" />
                <div className="login-card">
                    <Input placeHoleder={"example@gmail.com"} label="Gmail" type="text" handler= {this.gmailHandler}/>
                    <Input placeHoleder={""} label="Password" type="password" handler= { this.passHandler}/>
                    <Route render={({ history}) => (
                        <button
                            type='button'
                            onClick={() => { 
                                this.loginHandler();
                                if ( this.props.loading === false&& this.props.user !== undefined ){
                                   if(this.props.user.password === this.props.pass)
                                   {
                                       history.push('/home');
                                   }
                                }
                            }}
                            >
                            Login
                        </button>
                    )} />
                    <Route render={({ history}) => (
                        <button
                            type='button'
                            onClick={() => { history.push('/register') }}
                            >
                            Register
                        </button>
                    )} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        loading: state.userReducer.loadingUser,
        user : state.userReducer.user,
        pass: state.userReducer.loginPass,
        gmail: state.userReducer.loginGmail
    };
  };
  
  const mapDispatchToProps = (dispatch: any) => {
    return {
      fetchUser: (gmail: string) => {
        dispatch({type:"SET_LOADING_USER"})
        dispatch(fetchUser(gmail));
      },
      setGmail: (gmial: string) => {
          dispatch({type: "SET_LOGIN_GMIAL", payload: gmial})
      },
      setPass: (pass: string) => {
          dispatch({type: "SET_LOGIN_PASS", payload: pass})
      }
    };
  };
  
   export default connect(mapStateToProps, mapDispatchToProps)(Login);
