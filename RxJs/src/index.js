// Napraviti klasu MoviesService koja ima metode get koja vraca niz filmova(model klasa Movie), 
//getByIndex(index) koja po indeksu vraca film
//obe funkcije su asinhorone tj simuliracemo da se pozivaju sa servera
//vratiti fim po slucajnom indeksu, pri tome koristiti random fuk=nkciju sa ovog casa getRandomNumberAsync
// umesto jasno definisanih vremenskih intervala staviti Math.random()*1000
//imamo sekvencijalne asinhrone pozive koji se cekaju, vise then se koriste
// da uporedimo dva filma po oceni, sa asinhronom funkcijom
//paralelno slanje promisa

import {Movie} from "./Movie";
import {MovieService} from "./MovieService";
import { getRandomNumberAsync } from "./funkcije";


let arrayMovies=new MovieService("ja");
let movie=new Movie("Kill Bill",9.8);
arrayMovies.addMovie(movie);

movie=new Movie("Mister Bin",7.8);
arrayMovies.addMovie(movie);

movie=new Movie("Mister Bin 2",2.3);
arrayMovies.addMovie(movie);

movie=new Movie("Marko",5.5);
arrayMovies.addMovie(movie);

movie=new Movie("Marko 2",10.0);
arrayMovies.addMovie(movie);

console.log(arrayMovies);

getRandomNumberAsync(arrayMovies.size)
.then(index=>{
    return arrayMovies.getByIndex(index);
})
.then(movie=>console.log(movie))
.catch(reason=>console.log(reason));