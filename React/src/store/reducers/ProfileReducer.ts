interface User{
    id: number;
    name: string;
    password : string;
    gmail : string;
    userPic: string;
}
interface IProfileState {
    userProfile: User;
    profilePosts: any;
    profilePictures: any;
    showAddFrend: string;
}

const initialProfileState : IProfileState =  {
   
        userProfile: {
            id: 0,
            name: "",
            password : "",
            gmail : "",
            userPic: ""
        },
        profilePosts: [],
        profilePictures: [],
        showAddFrend: "add-frend closed"
} 

const profileReducer = (state = initialProfileState , action: any) => {
    switch(action.type){
        case "FETCH_PROFILE_USER":
            state = {
                ...state,
                userProfile: action.payload
            }
        break;
        case "SET_PROFILE_POST":
            state = {
                ...state,
                profilePosts: [...state.profilePosts ,action.payload]
            }
        break;
        case "SET_PROFILE_PICTURES":
            let pictures = action.payload.filter( (post: any) => post.picUrl !== ''); 
            state = {
                ...state,
                profilePictures: pictures
            }
        break;
        case "CLEAR_PROFILE":
            state = {
                ...state,
                profilePictures: [],
                profilePosts: []
            }
        break;
        case "ADD_FREND_VISIBLE":
            state = {
                ...state,
               showAddFrend: "add-frend"
            }
        break;
        case "ADD_FREND_NOT_VISIBLE":
            state = {
                ...state,
               showAddFrend: "add-frend closed"
            }
        break;

    }
    return state;
}

export default profileReducer;