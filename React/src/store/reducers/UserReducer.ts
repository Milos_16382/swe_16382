

// interface User{
//     id: number;
//     name: string;
//     password : string;
//     gmail : string;
//     userPic: string;
// }
// interface IUserState {
//     user : User;
//     posts: [];
//     loadingUser: boolean;
//     frends: [];
// }
interface User{
    id: number;
    name: string;
    password : string;
    gmail : string;
    userPic: string;
}
interface IUserState {
    user: User;
    resoults: any;
    loadingUser: boolean;
    frends: any;
    posts: any;
    lastPost: any;
    loginPass: string;
    loginGmail: string;
     sideDrawerOpen: boolean,
    searchShow: boolean,
    resoultsShow: boolean,
    postsUpdated:boolean
}

const initialUserState : IUserState =  {
        user: {
            id: 0,
            name: "",
            password : "",
            gmail : "",
            userPic: ""
        },
        resoults: [],
        loadingUser: true,
        frends: [],
        posts: [],
        lastPost: [],
        loginPass: "",
        loginGmail: "",
         sideDrawerOpen: false,
        searchShow: false,
        resoultsShow: false,
        postsUpdated:false
} 

const userReducer = (state = initialUserState , action: any) => {
    switch(action.type){
        case "SET_USER":
            state = {
                ...state,
                loadingUser: false,
                user: action.payload
            }
        break;
        
        case "SET_LOADING_USER":
            state = {
                ...state,
                loadingUser: true,
            }
        break;

        case 'SET_POSTS':
                state = {
                    ...state,
                    posts: [...state.posts,action.payload]
            }
        break;
        case 'SET_LAST_POSTS':
                state = {
                    ...state,
                    lastPost:[...state.lastPost,action.payload]
            }
        break;
        case 'CHANGE_LAST_POSTS':
            let lPost = [...state.lastPost];
            lPost[action.payload.change] = action.payload;
                state = {
                    ...state,
                    lastPost: lPost
            }
        break;
        case 'SET_FREND':
                state = {
                    ...state,
                    frends: [...state.frends,action.payload]
                }
        break;
        case 'SET_LOGIN_GMIAL':
                state = {
                    ...state,
                    loginGmail : action.payload
                }
        break;
        case 'SET_LOGIN_PASS':
                state = {
                    ...state,
                    loginPass: action.payload
                }
        break;
        case "SET_SEARCH_RESULTS":
            state = {
                ...state,
                resoults: action.payload
            }
        break;
        case "SET_SIDE_DRAWER_OPEN":
            state = {
                ...state,
                sideDrawerOpen: !state.sideDrawerOpen
            }
        break;
        case "SET_SIDE_DRAWER_CLOSE":
            state = {
                ...state,
                sideDrawerOpen: false
            }
        break;
        case "SET_SEARCH_SHOW":
            state = {
                ...state,
                searchShow: !state.searchShow,
                resoultsShow: false
            }
        break;
        case "SET_RESOULTS_SHOW":
            state = {
                ...state,
                resoultsShow: !state.resoultsShow
            }
        break;
        
        case "UN_SET_POST_UPDATE":
            state = {
                ...state,
                postsUpdated: false
            }
        break;
        case "SET_POST_UPDATE":
            state = {
                ...state,
                postsUpdated: true
            }
        break;
        case "ADD_LIKE":

            let posts = [...state.posts];
            posts.map( post => {
                if(post.id === action.payload) {
                    post.liked = 'fas fa-heart';
                    post.likes++;
                }
            })
            state = {
                ...state,
                posts
            }
        break;
        case "REMOVE_LIKE":
            let newPosts = [...state.posts];
            newPosts.map( post => {
                if(post.id === action.payload) {
                    post.liked = 'far fa-heart';
                    post.likes--;
                }
            })
            state = {
                ...state,
                posts: newPosts
            }
        break;
    }
    return state;
};


export default userReducer;