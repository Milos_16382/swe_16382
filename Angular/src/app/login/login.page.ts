import { Component, OnInit } from '@angular/core';
import { Color } from '@ionic/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import * as fromStore from '../store';
import { Observable } from 'rxjs';
import { map, skip } from 'rxjs/operators';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userIndex: number;
  userPassword: string;
  userFax: number;
  isDisabled = true;

  provera: any;
  errPass: Color = 'danger';
  errIndex: Color = 'danger';

  user: Observable<User>;
  loging = true;
  constructor(private router: Router, private toastCtrl: ToastController,
              private store: Store<fromStore.CBState>) { }

  ngOnInit() {
  }
  loginSetup() {
    this.store.dispatch(new fromStore.LoginUser({
      Name: '',
      Password: this.userPassword,
      Fax: this.userFax,
      id: this.userIndex,
      PicUrl: ''
  }));

    this.login();
  }
  login() {

    this.user = this.store.select(fromStore.getUser);
    this.store.dispatch(new fromStore.GetUser({
      index: this.userIndex,
      fax: this.userFax
    }));
    this.user.pipe(
      skip(1),
      map( user => {
        if ( Object.keys(user).length > 0 && user.Password === this.userPassword) {
          setTimeout( () => {
            this.loging = false;
          }, 200);
        } else {
          this.loginToastErr();
        }
      })
    ).subscribe();

  }

  indexCheck() {
    if (this.userIndex !== null && this.userIndex !== undefined && this.userIndex !== 0 && this.userFax !== undefined) {
      this.errIndex = 'primary';
    }

    if (this.errPass === 'primary' && this.errIndex === 'primary') {
      this.isDisabled = false;
    }
  }
  passCheck() {
    if (this.userPassword !== null && this.userPassword !== undefined && this.userPassword !== '' && this.userFax !== undefined) {
      this.errPass = 'primary';
    }

    if (this.errPass === 'primary' && this.errIndex === 'primary') {
      this.isDisabled = false;
    }
  }
  async loginToastErr() {
    const toast = await this.toastCtrl.create({
      message: 'Incorect Username or Password',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();

  }
}
