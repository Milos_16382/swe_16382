import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import * as fromStore from './store';
import { map } from 'rxjs/operators';
import { UserServiceService } from './user-service.service';
@Injectable({
  providedIn: 'root'
})
export class ColectionServiceService {


  searchUser$: Observable<User>;
  user$: Observable<User>;
  searching: Promise<boolean>;
  url = 'http://localhost:3000';
  httpHeaders = new HttpHeaders({
    Accept: 'application/json',
    'Content-Type': 'application/json'
  });
  options = {
    headers: this.httpHeaders
  };
  constructor(private http: HttpClient, private store: Store<fromStore.CBState>, private userService: UserServiceService) {
    // this.searchUser$ = this.store.select(fromStore.getLoginUser);
    // this.user$ = this.store.select(fromStore.getUser);
    // this.searching = this.store.select(fromStore.getSearching).toPromise();
   }

  getColection(index: number, fax: number): Observable<Colection[]> {


    const col = this.http.get<Colection[]>(`${this.url}/colection?IndexUser=${index}&FaxUser=${fax} `, this.options)
    .pipe(
      map( colection => {
        colection.map( singleCol => {

          this.liked(singleCol.id, index, fax).then(
            array => {
              if ( array[0] === null || array[0] === undefined) {
                singleCol.Liked = 'heart-empty';
              } else {
                singleCol.Liked = 'heart';
              }
            }
          );
          this.likes(singleCol.id, index, fax).then( array => {
            let nmb = 0;
            array.map( () => nmb++);
            singleCol.Likes = nmb;
          });
          return singleCol;
        });
        return colection;
      })
    );
    col.subscribe();

    return col;

  }
  editColection(col: Colection) {

    this.http.put<Colection[]>(`${this.url}/colection/${col.id}`, col, this.options).subscribe();

  }
  getComents(col: number): Observable<Coments[]> {

    const ret = this.http.get<Coments[]>(`${this.url}/coment?IdCol=${col}`, this.options).pipe(
      map( coments => {
        coments.map( coment => {
          const user$ = this.userService.getUser(coment.IndexUser, coment.FaxUser);
          user$.subscribe( user => {
            coment.NameUser = user[0].Name;
            coment.UserPic = user[0].PicUrl;
          });
          return coment;
        });
        return coments;
      })
    );
    return ret;
  }
  setComent(coment: Coments) {

    this.http.post(`${this.url}/coment`, coment, this.options).subscribe();
  }
  addElement(col: Colection) {

    this.http.post<Colection[]>(`${this.url}/colection`, col, this.options).subscribe();

  }
  deleteColection(i: number) {

    this.http.delete(`${this.url}/colection/${i}`, this.options).subscribe();
  }
  like(col: number, liked: string, index: number, fax: number) {

    console.log('BRISIII', liked);
    if (liked === 'heart') {
      this.http.get<any[]>(`${this.url}/like?userIndex=${index}&userFax=${fax}&colId=${col}`, this.options).subscribe(
        el => el.map( x => {

          this.http.delete(`${this.url}/like/${x.id}`, this.options).subscribe();
        }))
      ;

    } else {
      this.http.post(`${this.url}/like`, {
      userIndex: index,
      userFax: fax,
      colId: col
      }, this.options).subscribe();
    }

  }
  likes(col: number, index: number, fax: number) {

    return this.http.get<any[]>(`${this.url}/like?colId=${col}`, this.options).toPromise();
  }
  liked(col: number, index: number, fax: number) {

    return this.http.get<any[]>(`${this.url}/like?userIndex=${index}&userFax=${fax}&colId=${col}`, this.options).toPromise();
  }
  checkName(name: string) {
    return true;
  }
}
