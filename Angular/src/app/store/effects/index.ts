import {UserEffects} from './user.effect';
import {ColectionEffects} from './colection.effects';
import { CardEffects } from './card.effects';

export const effects: any[] = [UserEffects, ColectionEffects, CardEffects];

export * from './user.effect';
export * from './colection.effects';
export * from './card.effects';

