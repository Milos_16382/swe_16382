import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserServiceService } from './user-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardServiceService {

  kartice: Observable<Card[]>;
  url = 'http://localhost:3000';
  httpHeaders = new HttpHeaders({
    Accept: 'application/json',
    'Content-Type': 'application/json'

  });
  options = {
    headers: this.httpHeaders
  };
  constructor(private http: HttpClient) { }

  getCards(colId: number): Observable<Card[]> {

    return this.http.get<Card[]>(`${this.url}/card?ColectionParent=${colId}`, this.options);
  }
  addCard(card: Card, col: number) {

    this.http.post<Card[]>(`${this.url}/card`, card, this.options).subscribe();
  }
  deleteCard(index: number) {

    this.http.delete(`${this.url}/card/${index}`, this.options).subscribe();
  }

  edit(cardData: Card, id: number) {
    this.http.put<Card>(`${this.url}/card/${id}`, cardData, this.options).subscribe();

  }
  add(card: Card) {
    this.http.post<Card>(`${this.url}AddUser`, card, this.options).subscribe();
  }
}
