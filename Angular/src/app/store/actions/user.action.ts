import { Action} from '@ngrx/store';

export const GET_USER = 'get user';
export const GET_USER_SUCCESS = 'get user success';
export const SET_SEARCHING = 'set searching';
export const SET_USER = 'set user';
export const SET_SEARCH_USER = 'set search user';
export const SET_LOGIN_USER = 'set login userUser';
export const REGISTER_USER = 'register user';
export const UPDATE_USER = 'update user';
export const SEARCH_USER_NAME = 'search user name';
export const SEARCH_USER_NAME_SUCCESS = 'search user name success';

export class GetUser implements Action {
    readonly type = GET_USER;
    constructor(public payload: {
        index: number,
        fax: number
    }){}
}

export class GetUserSuccess implements Action {
    readonly type = GET_USER_SUCCESS;
    constructor(public payload: User) {}
}
export class SetSearching implements Action {
    readonly type = SET_SEARCHING;
    constructor(public payload: boolean) {}
}

export class SetUser implements Action {
    readonly type = SET_USER;
    constructor(public payload: User) {}
}

export class SetSearchUser implements Action {
    readonly type = SET_SEARCH_USER;
    constructor(public payload: User) {}
}

export class RegisterUser implements Action {
    readonly type = REGISTER_USER;
    constructor(public payload: User) {}
}
export class UpdateUser implements Action {
    readonly type = UPDATE_USER;
    constructor(public payload: User) {}
}
export class SearchUserName implements Action {
    readonly type = SEARCH_USER_NAME;
    constructor(public payload: string) {}
}
export class SearchUserNameSuccess implements Action {
    readonly type = SEARCH_USER_NAME_SUCCESS;
    constructor(public payload: User[]) {}
}
export class LoginUser implements Action {
    readonly type = SET_LOGIN_USER;
    constructor(public payload: User) {}
}
export type UserAction = GetUser | GetUserSuccess | SetSearching | SetUser | SetSearchUser | RegisterUser |
            UpdateUser | SearchUserName | SearchUserNameSuccess | LoginUser;
