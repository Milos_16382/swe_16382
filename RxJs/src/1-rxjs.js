import{interval, range, Observable, Subscription, Subject, fromEvent} from "rxjs";
import{take,filter,map,takeUntil,sampleTime,from, debounceTime,switchMap} from "rxjs/operators";


function execInterval(){
interval(500).pipe(
    filter(x => x%2 === 1),
    map( x => x + 2),
    take(10)).subscribe(x => console.log(x));
}
function execRange(){
range(10,30).subscribe(x => console.log(x));
}

function getRandomNumbers(){
    return Observable.create( generator => {
      setInterval( () =>{ generator.next(parseInt(Math.random() * 10));
      },500);
    });
}
function writeTenEvanNumbers(){

getRandomNumbers().pipe(
    filter( x => x % 2 === 0),
    take(10)
).subscribe( x => console.log(x.id));
}

  
  
