import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CardServiceService } from '../../card-service.service';
import { ToastController, ModalController, AlertController } from '@ionic/angular';
import { CardBackComponent } from 'src/app/card-back/card-back.component';
import { Observable } from 'rxjs';
import { CardEditComponent } from 'src/app/card-edit/card-edit.component';
import { UserServiceService } from 'src/app/user-service.service';
import * as fromStore from '../../store';
import { Store } from '@ngrx/store';
import { skip } from 'rxjs/operators';


@Component({
  selector: 'app-card-page',
  templateUrl: './card-page.page.html',
  styleUrls: ['./card-page.page.scss'],
})
export class CardPagePage implements OnInit {

  cards$: Observable<Card[]>;
  cardEntities: Observable<{[id: number]: Card}> ;
  coments$: Observable<Coments[]>;
  searchUser$: Observable<User>;
  user$: Observable<User>;
  searching$: Observable<boolean>;
  colectionId: number;
  comentText: string;
  slide = 0;
  znanje: number;
  constructor(private activatedRoute: ActivatedRoute  , private toastCtrl: ToastController
    ,         private modalController: ModalController, private userService: UserServiceService,
              private alertController: AlertController, private store: Store<fromStore.CBState>) {
    }

    ngOnInit() {
      this.activatedRoute.paramMap.subscribe(param => {
        if (!param.has('colectionId')) {
          return;
        }
        // tslint:disable-next-line: radix
        this.colectionId = parseInt(param.get('colectionId'));
        this.searchUser$ = this.store.select(fromStore.getLoginUser);
        this.user$ = this.store.select(fromStore.getUser);
        this.searching$ = this.store.select(fromStore.getSearching);
        this.cards$ = this.store.select(fromStore.getCards);
        this.cardEntities = this.store.select(fromStore.getCardEnities);
        this.coments$ = this.store.select(fromStore.getComents);

        this.store.dispatch(new fromStore.GetCards(this.colectionId));


        this.store.dispatch(new fromStore.GetComents(this.colectionId));

        this.cards$.pipe(skip(1)).subscribe(cardss => {
        if (cardss.length === 0) {
          if (this.searchCheck()) {
            this.makeCardToast();
          }
        }
        let znanjee = 0;
        cardss.map(x => {
          if (x.Learned === true) {
            znanjee++;
          }
        });
        this.znanje = Math.round(znanjee / cardss.length * 100);
      });

    });
  }
  displayBack(card: number) {
    this.cardEntities.subscribe( entities => this.cardBack(entities[card]));

  }
  async cardEdit() {
    const modal = await this.modalController.create({
      component: CardEditComponent,
      backdropDismiss: false
    });

    modal.present();
    return await modal.onDidDismiss();

  }
  async cardBack(card: any) {
    const modal = await this.modalController.create({
      component: CardBackComponent,
      componentProps: {
        answer: card.BackText,
        BackImg: card.BackImg
      }
    });

    modal.present();
    modal.onDidDismiss().then(x => modal.remove());
  }
  async makeCardToast() {
    const toast = await this.toastCtrl.create({
      message: 'Click the Setings button to add your first card',
      duration: 7000,
      position: 'top'
    });
    toast.present();
  }

  addCard(i: number) {
    this.store.dispatch(new fromStore.AddCard({
      card: {
        id: 0,
        ColectionParent: this.colectionId,
        FrontImg: "",
        FrontText: " ",
        BackImg: "",
        BackText: " ",
        Learned: false,
      },
      id: this.colectionId
    }));

  }

  deleteCard() {
    this.cards$.forEach(element => {
      this.store.dispatch(new fromStore.DeleteCard(element[this.slide].id));
    });
  }


  async editCard() {
    const data = await this.cardEdit();
    console.log(data);
    if (data !== undefined) {

      this.cards$.subscribe(element => {
        this.store.dispatch(new fromStore.UpdateCard({
          card: {
            id: 0,
            ColectionParent: this.colectionId,
            FrontImg: data.data.FrontImg,
            FrontText: data.data.FrontText,
            BackImg: data.data.BackImg,
            BackText: data.data.BackText,
            Learned: false,
          },
          id: element[this.slide].id
        }));
      });
      this.store.dispatch(new fromStore.GetCards(this.colectionId));
    }
  }
  makeComent() {


    let index: number;
    let fax: number;

    let search: boolean;
    this.user$.subscribe( user => {
      index = user[0].id;
      fax = user[0].Fax;
    });
    this.searching$.subscribe( searching => search = searching);
    this.store.dispatch(new fromStore.AddComent({
      id: 0,
      Coment: this.comentText,
      IndexUser: index,
      IdCol: this.colectionId,
      FaxUser: fax,
      NameUser: '',
      UserPic: ''
    }));
    this.store.dispatch(new fromStore.GetComents(this.colectionId));

  }

  slideLeft() {
    this.slide--;
  }

  slideRight() {
    this.slide++;
  }
  doRefresh(event) {

    this.store.dispatch(new fromStore.ClearCards());
    this.store.dispatch(new fromStore.GetCards(this.colectionId));
    this.store.dispatch(new fromStore.GetComents(this.colectionId));
    event.target.complete();
  }
  async makeEditToast() {
    const toast = await this.toastCtrl.create({
      message: 'Click Setings button then the edit button to add a Question',
      duration: 7000,
      position: 'top'
    });
    toast.present();
  }
  cardLearned() {
    this.cards$.subscribe(element => {
      this.store.dispatch(new fromStore.LearnCard({
        card: {
          id: 0,
          ColectionParent: this.colectionId,
          FrontImg: element[this.slide].FrontImg,
          FrontText: element[this.slide].FrontText,
          BackImg: element[this.slide].BackImg,
          BackText: element[this.slide].BackText,
          Learned: true,
        },
        id: this.colectionId
      }));
    });
  }
  async deleteAlert() {
    const alert = await this.alertController.create({
      header: 'Are you sure!',
      message: '<strong>you want to delete this card</strong>!!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary'

        }, {
          text: 'Yes',
          handler: () => {
            this.deleteCard();
          }
        }
      ]
    });

    await alert.present();
  }
  searchCheck(): boolean {
    let check: boolean;
    this.store.select(fromStore.getSearching).subscribe(search => {
      if (search === false) {
        check = true;
      } else {
        check = false;
      }
    });
    return check;
  }
}
