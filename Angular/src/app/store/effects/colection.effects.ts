import { Injectable } from '@angular/core';
import {Effect, Actions, ofType} from '@ngrx/effects';
import * as colectionActions from '../actions/colection.actions';
import { switchMap, map } from 'rxjs/operators';
import { ColectionServiceService } from 'src/app/colection-service.service';

@Injectable()
export class ColectionEffects {
    constructor(private actions$: Actions, private colectionService: ColectionServiceService) {}

    @Effect()
    getColections$ = this.actions$.pipe(
        ofType(colectionActions.GET_COLECTIONS),
        switchMap( (action: colectionActions.GetColections) => {
            return this.colectionService.getColection(action.payload.index, action.payload.fax).pipe(
                map( col => new colectionActions.GetColectionsSuccess(col))
            );
        })
    );
    @Effect()
    getComents$ = this.actions$.pipe(
        ofType(colectionActions.GET_COMENTS),
        switchMap( (action: colectionActions.GetComents) => {
            return this.colectionService.getComents(action.payload).pipe(
                map( coment => new colectionActions.GetComentsSuccess(coment))
            );
        })
    );
    @Effect({ dispatch: false })
    addColection$ = this.actions$.pipe(
      ofType(colectionActions.ADD_COLECTION),
      map( (action: colectionActions.AddColection) => {
          return this.colectionService.addElement(action.payload);
      })
    );
    @Effect({ dispatch: false })
    deleteColection$ = this.actions$.pipe(
        ofType(colectionActions.DELETE_COLECTION),
        map( (action: colectionActions.DeleteColection) => {
            return this.colectionService.deleteColection(action.payload);
        })
      );
    @Effect({ dispatch: false })
    editColection$ = this.actions$.pipe(
        ofType(colectionActions.EDIT_COLECTION),
        map( (action: colectionActions.EditColection) => {
            return this.colectionService.editColection(action.payload);
        })
      );

    @Effect({ dispatch: false })
    setComent$ = this.actions$.pipe(
        ofType(colectionActions.ADD_COMENT),
        map( (action: colectionActions.AddComent) => {
            return this.colectionService.setComent(action.payload);
        })
      );
    @Effect({ dispatch: false })
    like$ = this.actions$.pipe(
        ofType(colectionActions.LIKE),
        map( (action: colectionActions.Like) => {
            return this.colectionService.like(action.payload.colId, action.payload.colLiked, action.payload.uIndx, action.payload.ufax);
        })
      );
}
