const path = require('path');

module.exports = {
  entry: './src/Main.js',
  
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath:'dist',
    filename: 'bundle.js'
  }
};