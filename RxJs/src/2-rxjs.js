import{interval, range, Observable, Subscription, Subject, fromEvent,from} from "rxjs";
import{take,filter,map,takeUntil,sampleTime, debounceTime,switchMap} from "rxjs/operators";


function subscribeToOddNumbers(stop$){
 return interval(500).pipe(
    filter(x => x%2 === 1),
    map( x => x + 2),
    takeUntil(stop$)
    ).subscribe(x => console.log(x));
}

function createUnsubscribeButton(subscrition){
    const button = document.createElement("button");
    document.body.appendChild(button);
    button.innerHTML = 'Unsub';
    button.onclick = () =>  subscrition.unsubscribe();

}
function createSubscribeButton(subscribe){
    const button = document.createElement("button");
    document.body.appendChild(button);
    button.innerHTML = 'Sub';
    button.onclick = () =>  subscrition.subscribe();
}
function createStopButton(stop$)
{
    const button = document.createElement("button");
    document.body.appendChild(button);
    button.innerHTML = 'Unsub';
    button.onclick = () => {
        stop$.next();
        stop$.complete();
    }
}
   // const subject$ = new Subject();
    //subject$.subscribe( x => console.log('sub,'+ x ));
    //subject$.next(4);
    //subject$.next(6);

function Mouse(stop$)
{
    document.onmousemove = (ev) => {
        console.log(' x = ${ex.x} y${ev.y}');
    }
}
fromEvent(document,'mousemove').pipe(
    map(event => ({
        x : event.x,
        y : event.y
    })),
    sampleTime(1000)
)
.subscribe( coordinets => console.log( `x = ${coordinets.x}, y${coordinets.y}`))
//const sub =  subscribeToOddNumbers(subject$);
//createStopButton(subject$);
//createUnsubscribeButton(sub);
//createSubscribeButton(sub);

//createSubject().subscribe( x => console.log('sub,'+x));

function getMovie(){
    return from(
        fetch(`http://localhost:3000/movies/${id}`)
    .then( response =>response.json())
    )
}

const textBox = document.createElement('input');
document.body.appendChild(textBox);


fromEvent(textBox,"input").pipe(
    debounceTime(500),
    map(ev => ev.target.value),
    filter( text =>  text.length >= 3),
    switchMap( id =>getMovie(id))
).subscribe( val => console.log(val))

getMovie('tito').subscribe(movie => console.log(movie));
//.then( movie => console.log(movie))
//.catch(error=> console.log(error))