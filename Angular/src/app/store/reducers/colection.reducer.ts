import * as fromCol from '../actions/colection.actions';
export interface ColectionState {
    Colections: {[id: number]: Colection};
    Coments: {[id: number]: Coments};
}

export const initialState: ColectionState = {
    Colections: {},
    Coments: {}
};

export function reducer(state = initialState, action: fromCol.ColectionAction): ColectionState {

    switch (action.type) {

        case fromCol.GET_COLECTIONS_SUCCESS: {
            const ColectionArray = action.payload;
            const Colections = ColectionArray.reduce( (Colections, colection: Colection) => {
                return {
                    ...Colections,
                    [colection.id] : colection
                };
            },
            { }
            );
            return {
                ...state,
                Colections
            };
        }
        case fromCol.GET_COMENTS_SUCCESS: {
            const ComentArrtay = action.payload;
            const Coments = ComentArrtay.reduce( (Coments, coment) => {
                return {
                    ...Coments,
                    [coment.id] : coment
                };
            },
            { }
            );
            return {
                ...state,
                Coments
            };
        }
        default : {
            return state;
        }
    }
}

export const getColections = (state: ColectionState) => state.Colections;
export const getComents = (state: ColectionState) => state.Coments;
