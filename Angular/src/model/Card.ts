
interface Card {
    id: number;
    ColectionParent: number;
    FrontImg: string;
    FrontText: string;
    BackImg: string;
    BackText: string;
    Learned: boolean;
}
