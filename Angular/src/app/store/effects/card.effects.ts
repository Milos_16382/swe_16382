import { Injectable } from '@angular/core';
import {Effect, Actions, ofType} from '@ngrx/effects';
import * as cardActions from '../actions/card.action';
import { switchMap, map } from 'rxjs/operators';
import { CardServiceService } from 'src/app/card-service.service';
import {GetCards} from '../actions/card.action';

@Injectable()
export class CardEffects {
    constructor(private actions$: Actions, private cardService: CardServiceService) {}

    @Effect()
    getCards$ = this.actions$.pipe(
        ofType(cardActions.GET_CARDS),
        switchMap( (action: GetCards) => {
            return this.cardService.getCards(action.payload).pipe(
                map( cards => new cardActions.GetCardsSuccess(cards))
            );
        })
    );

    @Effect({ dispatch: false })
    deleteCard$ = this.actions$.pipe(
        ofType(cardActions.DELETE_CARD),
        map( (action: cardActions.DeleteCard) => {
            return this.cardService.deleteCard(action.payload);
        })
    );

    @Effect({ dispatch: false })
    updateCard$ = this.actions$.pipe(
        ofType(cardActions.UPDATE_CARD),
        map( (action: cardActions.UpdateCard) => {
            return this.cardService.edit(action.payload.card, action.payload.id);
        })
    );

    @Effect({ dispatch: false })
    addCard$ = this.actions$.pipe(
        ofType(cardActions.ADD_CARD),
        map( (action: cardActions.AddCard) => {
            return this.cardService.addCard(action.payload.card, action.payload.id);
        })
    );

    @Effect({ dispatch: false })
    LearnCard$ = this.actions$.pipe(
        ofType(cardActions.LEARN_CARD),
        map( (action: cardActions.LearnCard) => {
            return this.cardService.edit(action.payload.card, action.payload.id);
        })
    );
}
