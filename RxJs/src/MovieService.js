export class MovieService{
    constructor(name){
        this.name=name;
        this.arrayOfMovies=[];
    }

    addMovie(movie){
        this.arrayOfMovies.push(movie);
    }

    returnArray(){
        return new Promise((resolve,reject)=>{
            const randomNumber=Math.random()*100;
            setTimeout(()=>resolve(this.arrayOfMovies),randomNumber); 
        })
    }

    get size(){
        return this.arrayOfMovies.length;
    }

    getByIndex(index){
        return new Promise((resolve,reject)=>{
            const randomNumber=Math.random()*100;
            setTimeout(()=>resolve(this.arrayOfMovies[index]),randomNumber); 
        })
    }
}