import { Action} from '@ngrx/store';

export const GET_COLECTIONS = 'get colections';
export const GET_COLECTIONS_SUCCESS = 'get colections success';
export const ADD_COLECTION = 'add colection';
export const EDIT_COLECTION = 'edit colection';
export const DELETE_COLECTION = 'delete colection';
export const ADD_COMENT = 'add coment';
export const GET_COMENTS = 'get coments';
export const GET_COMENTS_SUCCESS = 'get coments success';
export const LIKE = 'like';

export class GetColections implements Action {
    readonly type = GET_COLECTIONS;
    constructor(public payload: {
        index: number,
        fax: number
    }) {}
}

export class GetColectionsSuccess implements Action {
    readonly type = GET_COLECTIONS_SUCCESS;
    constructor(public payload: Colection[]) {}
}

export class AddColection implements Action {
    readonly type = ADD_COLECTION;
    constructor(public payload: Colection) {}
}
export class EditColection implements Action {
    readonly type = EDIT_COLECTION;
    constructor(public payload: Colection
    ) {}
}
export class DeleteColection implements Action {
    readonly type = DELETE_COLECTION;
    constructor(public payload: number) {}
}
export class AddComent implements Action {
    readonly type = ADD_COMENT;
    constructor(public payload: Coments) {}
}
export class GetComents implements Action {
    readonly type = GET_COMENTS;
    constructor(public payload: number) {}
}
export class GetComentsSuccess implements Action {
    readonly type = GET_COMENTS_SUCCESS;
    constructor(public payload: Coments[]) {}
}
export class Like implements Action {
    readonly type = LIKE;
    constructor(public payload: {
        colId: number,
        colLiked: string,
        uIndx: number,
        ufax: number
    }) {}
}
export type ColectionAction = GetColections | GetColectionsSuccess | AddColection | EditColection | DeleteColection
| Like | AddComent | GetComents | GetComentsSuccess;
