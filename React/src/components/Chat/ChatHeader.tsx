import React from 'react';
import './ChatHeader.css';
import Avatar from '../Avatar/Avatar';
import { Route } from 'react-router';

interface Props {
    hide: any;
    close:any;
    closed: any;
    pic: string;
    name: string;
    userId: number;
}
const chatHeader = (props: Props) => (
    <div className={`chat-header ${props.closed}`} >
        <Avatar size="small" link={props.pic} />
        <Route render={({ history}) => (
                        <p onClick={() => history.push(`/home/${props.userId}`)}> {props.name}</p>
                    )} />
        
        <div className='spacer' onClick={props.hide}/>
        <i onClick={props.close} className="fas fa-times"></i>
 
    </div>
);

export default chatHeader;