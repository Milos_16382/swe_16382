import React, { Component } from 'react';
import './ChatBox.css';
import ChatMsg from './ChatMsg';
import ChatHeader from '../ChatHeader';
import { connect } from 'react-redux';
import { updateChat, sendMessage } from '../../../store/actions/ChatActions';

interface Props {
    close: string;
    hide: string;
    sent: boolean;
    setHide: any;
    setClose: any;
    user: any;
    chatUser: any;
    chat: any;
    updateChat: any;
    send: any;
    resetSendComplete: any;
    setMsg: any;
    clearMsg: any;
    message: string;
}

class chatBox extends Component<Props>{

    state = {
        message : ''
    }
    hideHenddler = () => {
        this.props.setHide();
    }
    closeHandler = () => {
        this.props.setClose();
    }
    inputHandler = (event: any) => {
        this.props.setMsg(event.target.value);
    }
    sendHandler = () => {
        this.props.resetSendComplete();
        this.props.send(this.props.user.id,this.props.chatUser.id, this.props.message);
        this.props.clearMsg()
        //this.props.updateChat( parseInt(`${Today.getHours()}${Today.getMinutes()}`),this.props.chat[this.props.chat.length-1].id,this.props.user.id, this.props.chatUser.id)
    }
    LoadMoreHandler = () => {
        if(this.props.chat[0] != undefined)
        this.props.updateChat(this.props.chat[this.props.chat.length-1].id,this.props.user.id, this.props.chatUser.id);
    }
    render(){ 

        if(this.props.sent){
            console.log(this.props.chat[0].id, "chat id");
            //this.props.updateChat(this.props.chat[this.props.chat.length-1].id,this.props.user.id, this.props.chatUser.id);
        }

        let message = this.props.chat.map(  (poruka:any) =>{
            if(poruka.sender === this.props.chatUser.id){
                return (<ChatMsg  person="chat friend" message={poruka.message} url={this.props.chatUser.userPic} key={poruka.id}/>);
            }else{
                return (<ChatMsg  person="chat self" message={poruka.message} url={this.props.user.userPic} key={poruka.id}/>);
            }
        });
        return(
    <div className={`chatbox ${this.props.hide} ${this.props.close}`}>
        <ChatHeader userId={this.props.chatUser.id} closed={this.props.close} hide={this.hideHenddler} close={this.closeHandler} pic={this.props.chatUser.userPic} name={this.props.chatUser.name}/>
        <div className={`chatlogs ${this.props.hide}`}>
        {message}
        <p className="load-more"  onClick={this.LoadMoreHandler}>Load More Messages </p>
        </div>
        <div className={`chat-form ${this.props.hide}`}>
                <textarea onChange={(event) => this.inputHandler(event)} value={this.props.message}></textarea>
                <button onClick={this.sendHandler}>Send</button>
            </div>
    </div>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        hide : state.chatReducer.chatHide,
        close : state.chatReducer.chatClose,
        chatUser : state.chatReducer.chatUser,
        user: state.userReducer.user,
        chat: state.chatReducer.chat,
        sent: state.chatReducer.sendCompleete,
        message: state.chatReducer.message
    };
  };
  
  const mapDispatchToProps = (dispatch: any) => {
    return {
      setHide: () => {
          dispatch({type: "SET_CHAT_HIDE"})
      },
      setClose: () => {
          dispatch({type: "SET_CHAT_CLOSED"})
      },
      updateChat: (id:number, mId: number, fId: number) => {
        dispatch(updateChat(mId,fId,id))
      },
      send: (mId: number, fId: number, msg: string) => {
          dispatch(sendMessage(mId,fId,msg))
      },
      resetSendComplete : () => {
          dispatch({type:"SEND_COMPLETE_RESET"});
      },
      setMsg: (msg: string) => {
        dispatch({type:"SET_MSG", payload: msg});
      },
      clearMsg: () => {
        dispatch({type:"CLEAR_MSG"});
      }
    };
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(chatBox);
